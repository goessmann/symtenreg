import model_TT as moTT

import numpy as np
import pandas as pd

import helpers_nonlinear as hn
import helpers_analysis as ha

import thresholding_TT as trTT


#########
#TEST PARAMETERS
#########

nleg=4                              ## Order of the Coefficient Tensor
ndim=2                              ## Number of basisfunctions
ndat=100                           ## Number of snapshots
ran=3                               ## Rank of the Coefficient Tensor (TT)
outdim=1                            ## Number of outdimensions
magn=1                              ## Magnitude of the random initialized Coefficients
types=['mul','mul','mul']           ## Types of the contractions

strat="GD"                         ## Strategy of the optimization
fac=300                               ## Length of update
nit=200                              ## Number of iterations
size=100                          ## Datasize for update



## Generation of test data
gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
gen_mod.set_points_random()
gen_mod.set_datacores("legendre")

## Initialize coeffcores
c=np.zeros((nleg-1,ran,ran,ndim))
c[0,0,0]=magn*np.random.rand(ndim)
c[1,0,1]=magn*np.random.rand(ndim)
c[2,1,2]=magn*np.random.rand(ndim)

outcore=np.zeros((ran,outdim,ndim))
outcore[2,0]=magn*np.random.rand(ndim)
gen_mod.init_coeffcores(c,outcore)
y=gen_mod.forward_generation()

## Initialization of the regressor
mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
mod.set_points(gen_mod.points)
mod.set_datacores("legendre")
mod.init_coeffcores_random(magnitude=magn)
mod.forward_generation(start=0,comgradients=True)



p_his,c_his=mod.randomized_backward_iteration(y,size,nit,fac,strat,measure_coef_er=True,
                                      gen_mod=gen_mod)

#print(p_his,c_his)


print('First ten datapoints:')

print(y[:3],mod.generation[:3])


con_mod=ha.contract_cores(mod)
con_gen=ha.contract_cores(gen_mod)
core_diff=con_mod-con_gen
print('Norm of coefficient error, Norm of ground truth coefficients:',np.linalg.norm(core_diff),np.linalg.norm(con_gen))



## CREATE DATAFRAME
result=pd.DataFrame({'Pred_Error': p_his, 'Coef_Error': c_his})
result.to_csv("results/sparse_"+strat+'_'+str(nit)+'_'+str(size)+'_'+str(fac)+'.csv')







## CREATE TXT DATA
#result_p = open('results/sparse_p_'+strat+'_'+str(nit)+'_'+str(size)+'_'+str(fac)+'.txt','w')
#result_c = open('results/sparse_c_'+strat+'_'+str(nit)+'_'+str(size)+'_'+str(fac)+'.txt','w')

#for k in range(nit):
#    result_p.write(str(p_his[k])+' ')
#    result_c.write(str(c_his[k])+' ')

#result_p.close()
#result_c.close()


