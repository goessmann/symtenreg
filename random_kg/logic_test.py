import model_CP as moCP

from random_kg import recovery_experiment

from matplotlib import pyplot as plt
import numpy as np

nlegs = range(1,10)
ndats = range (1,100,1)

err_ar = np.empty((len(nlegs),len(ndats)))

for i  in range(len(nlegs)):
    for j in range(len(ndats)):
        nleg = nlegs[i]
        ndat = ndats[j]

        err_ar[i,j] = recovery_experiment.recovery_experiment(nleg,ndat,ndim=5,p=0.8)

plt.imshow(err_ar,cmap="coolwarm",vmin=0,vmax=1)
plt.title("Error Rate")

plt.ylabel("Number of Legs")
plt.yticks(range(len(nlegs)),nlegs)

plt.xlabel("Number of Data")
plt.xticks(range(len(ndats)),ndats)

plt.colorbar()
plt.show()

exit()

nleg = 5
ndat = 10
ndim = 5

p = 0.5


recovery_experiment.recovery_experiment(nleg,ndat,ndim,p)

