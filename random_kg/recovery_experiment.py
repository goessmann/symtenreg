import model_CP as moCP
import numpy as np

def recovery_experiment(nleg,
                        ndat,
                        ndim,
                        p):
    ran = 1
    outdim = 1

    datacores = np.random.binomial(1,p,size=(nleg,ndim,ndat))

    true_coeffs = np.zeros((nleg, outdim, ndim, ran))
    true_positions = np.empty(nleg)
    for k in range(nleg):
        random_pos = np.random.randint(0, ndim - 1)
        true_positions[k] = random_pos
        true_coeffs[k, 0, random_pos, 0] = 1

    ## Generate Coefficients
    gen_mod = moCP.symbolic_model_CP(nleg = nleg,
                       ndat = ndat,
                       ndim = ndim,
                       ran = ran,
                       outdim = outdim)
    gen_mod.datacores = datacores
    gen_mod.coeffcores = true_coeffs

    y = gen_mod.com_prediction_linear()

    ## Initialize Recovery Model
    rec_mod = moCP.symbolic_model_CP(nleg = nleg,
                       ndat = ndat,
                       ndim = ndim,
                       ran = ran,
                       outdim = outdim)
    rec_mod.datacores = datacores

    rec_mod.coeffcores = np.zeros((nleg, outdim, ndim, ran))
    for k in range(nleg):
        random_pos = np.random.randint(0, ndim - 1)
        rec_mod.coeffcores[k, 0, random_pos, 0] = 1

    ## Fit to random data
    itnum = 10
    for iteration in range(itnum):
        rec_mod.backward_iteration(y, size=ndat, com_error=False)

    recovered_positions = np.empty(nleg)
    for k in range(nleg):
        recovered_positions[k] = np.argmax(rec_mod.coeffcores[k])

    er_count = len(np.nonzero(true_positions - recovered_positions)[0])

    print("False Coefficients: {}".format(er_count))
    print("Correct Recovered: {}".format(nleg - er_count))

    return er_count/nleg