import model_TT as moTT

import numpy as np
import helpers_nonlinear as hn

#########
#TEST PARAMETERS
#########

nleg=2      ## Order of the Coefficient Tensor
ndim=2      ## Number of basisfunctions
ndat=100    ## Number of snapshots
ran=2       ## Rank of the Coefficient Tensor (TT)
magn=1      ## Magnitude of the random initialized Coefficients
#types=['mul','mul','mul','mul','mul','mul','mul','mul','plus','plus','plus']
types=['ReLU','ReLU','ReLU']

            ## Types of the contractions


## Initialization of the regressor
mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
mod.set_points_random()
mod.set_datacores_legendre()
mod.init_coeffcores_random(magnitude=magn)
mod.forward_generation(start=0,comgradients=True)

## Generation of test data
gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
gen_mod.set_points(mod.points)
gen_mod.set_datacores_legendre()
gen_mod.init_coeffcores_random(magnitude=magn)
y=gen_mod.forward_generation()

## Optimization of the regressor
mod.backward_iteration(y,10000,strategy='GD',lam=0.001)
print(y[:10],mod.generation[:10])


