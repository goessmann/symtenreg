import model_HT as moHT

import numpy as np
import helpers_nonlinear as hn

#########
#TEST PARAMETERS
#########
nleg=10
ndim=4      ## Number of basisfunctions
ndat=1    ## Number of snapshots
types=['mul','mul','mul','mul','mul','mul','mul','mul','plus','plus','plus']
            ## Types of the contractions
coeffnumber=3
transnumber=10
ran=2


#tensor=np.random.rand(2,3,1)
#co=moHT.core(1,0,0,0,types,tensor)

#infu1=np.array([[0,0]])
#infu2=np.array([[1,0,0]])
#print(tensor)
#print(co.compute_function(infu1,infu2))
#print(co.compute_gradient2(infu1,infu2)[0,1])
#print(np.tensordot(infu2,tensor,axes=[1,1])[0,:,0])


## Initialization of the regressor
mod=moHT.symbolic_model_HT(nleg,ndat,ndim,types)

coeffcores=np.random.rand(coeffnumber,ran,ran,ran)
transmatrices=np.random.rand(transnumber,ran,ran)

neighbores=np.zeros((coeffnumber+transnumber,3))
for k in range(coeffnumber):
    neighbores[k,0]=0



mod.init_coeffcores(coeffcores,transmatrices,neighbores)

print(mod.coeffnumber)
#mod.set_points_random()
#mod.set_datacores('legendre')
#mod.init_coeffcores_random()
#mod.forward_generation(start=0,comgradients=True)



