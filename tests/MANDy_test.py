import model_TT as moTT

import numpy as np
import helpers_nonlinear as hn
import helpers_analysis as ha

#########
#TEST PARAMETERS
#########

nleg=4      ## Order of the Coefficient Tensor
ndim=2      ## Number of basisfunctions
ndat=1000    ## Number of snapshots
ran=2       ## Rank of the Coefficient Tensor (TT)
magn=1      ## Magnitude of the random initialized Coefficients
types=['ReLU','mul','mul','mul','mul','mul','mul','mul','plus','plus','plus']
outdim=2
#types=['ReLU','ReLU']
            ## Types of the contractions

## Initialization of the regressor
mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
mod.set_points_random()
mod.set_datacores("fourier")
mod.init_coeffcores_random(magnitude=magn)
mod.forward_generation(start=0,comgradients=True)



## Generation of test data
gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
gen_mod.set_points(mod.points)
gen_mod.set_datacores("fourier")
gen_mod.init_coeffcores_random(magnitude=magn)
y=gen_mod.forward_generation()

## Optimization of the regressor
mod.backward_iteration(y,1000,strategy='GD',lam=0.01)
#print(mod.gradient_A_out[1,:,0,:])
#print(mod.gradient_A_out[1,:,1,:])
print(y[:3],mod.generation[:3])

con_mod=ha.contract_cores(mod)
con_gen=ha.contract_cores(gen_mod)
core_diff=con_mod-con_gen
print('Norm of coefficient error, Norm of ground truth coefficients:',np.linalg.norm(core_diff),np.linalg.norm(con_gen))

