import model_TT as moTT

import numpy as np
import helpers_nonlinear as hn
import helpers_analysis as ha



#########
#TEST PARAMETERS
#########

nleg=4      ## Order of the Coefficient Tensor
ndim=2      ## Number of basisfunctions
ndat=1000    ## Number of snapshots
ran=3       ## Rank of the Coefficient Tensor (TT)
outdim=1
magn=1      ## Magnitude of the random initialized Coefficients
#types=['mul','mul','mul','mul','mul','mul','mul','mul','plus','plus','plus']
types=['mul','mul','mul']

            ## Types of the contractions


## Initialization of the regressor
mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
mod.set_points_random()
mod.set_datacores("legendre")
mod.init_coeffcores_random(magnitude=magn)
mod.forward_generation(start=0,comgradients=True)




## Generation of test data
gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
gen_mod.set_points(mod.points)
gen_mod.set_datacores("legendre")

## Initialize coeffcores
c=np.zeros((nleg-1,ran,ran,ndim))
c[0,0,0]=magn*np.random.rand(ndim)
c[1,0,1]=magn*np.random.rand(ndim)
c[2,1,2]=magn*np.random.rand(ndim)

outcore=np.zeros((ran,outdim,ndim))
outcore[2,0]=magn*np.random.rand(ndim)
#c[4,0,0]=magn*np.random.rand(ndim)
gen_mod.init_coeffcores(c,outcore)
y=gen_mod.forward_generation()


mod.randomized_backward_iteration(y,1000,100,10,"GD")

## Optimization of the regressor
#mod.backward_iteration(y,10,strategy='GD',lam=0.1)


#for j in range(ndat):
#    if abs(mod.generation[j]-y[j])>0.01:
#        print('Outlier:')
#        print(j,mod.generation[j],y[j])

#c_recovered=mod.coeffcores
#for l in range(nleg):
#    for l1 in range(ran):
#        for l2 in range(ran):
#            if np.linalg.norm(c_recovered[l,l1,l2])>0.01:
#                print(l,l1,l2)
#                print(c_recovered[l,l1,l2])
#                print(c[l,l1,l2])

mod.ndat=ndat
mod.datacores=np.zeros((mod.nleg,mod.ndim,mod.ndat))
mod.set_datacores(mod.datatype,withoutone=True)
mod.partfunctions=np.zeros((mod.nleg-1,mod.ndat,mod.ran))
mod.forward_generation(comgradients=False)


print('First ten datapoints:')

print(y[:3],mod.generation[:3])

#        print(mod.generation[j],y[j])

con_mod=ha.contract_cores(mod)
con_gen=ha.contract_cores(gen_mod)
core_diff=con_mod-con_gen
print('Norm of coefficient error, Norm of ground truth coefficients:',np.linalg.norm(core_diff),np.linalg.norm(con_gen))
