import model_TT as moTT
import thresholding_TT as trTT

import numpy as np
import math

import helpers_nonlinear as hn
import helpers_analysis as ha


"""
Iterative Hard Tresholding for linear measurement operator A and X in the TT format: min(l2(AX-y)) subject to X low rank
    1) W^(j+1)=X^j + A^*[y-A(X^j)] -> fixpoint_iteration_linear()
    2) X^(j+1)=H_r(W^(j+1)) -> treshold_linear(): optimize low rank TT to approximate W
"""



#########
#TEST PARAMETERS
#########

nleg=3     ## Order of the Coefficient Tensor
ndim=2      ## Number of basisfunctions
ndat=20    ## Number of snapshots
ran=2       ## Rank of the Coefficient Tensor (TT)
magn=1      ## Magnitude of the random initialized Coefficients
types=["mul",'mul','mul','mul','mul','mul','mul','mul','plus','plus','plus']
outdim=4
size=20
nit=100
lam=1

## Initialization of the regressor
mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
mod.set_points_random()
mod.set_datacores("legendre")
mod.init_coeffcores_random(magnitude=magn)
mod.forward_generation(start=0,comgradients=True)

## Generation of test data
gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
gen_mod.set_points(mod.points)
gen_mod.set_datacores("legendre")
gen_mod.init_coeffcores_random(magnitude=magn)
y=gen_mod.forward_generation()



trTT.iterative_hard_tresholding_TT_mandy(mod,y,10,1000,trlength=1,genmod=gen_mod,genknown=True)


#mod.randomized_backward_iteration(y,size,nit,lam,strategy="IHT_lin",measure_coef_er=True,gen_mod=gen_mod)

#appmod=iterative_hard_treshold_TT(mod,y,100,10)
#for j in range(ndat):
#    print(np.linalg.norm(mod.points[j]),np.linalg.norm(mod.generation[j]))

#ha.geometry_change_parameter(mod,50)



#modcon=ha.contract_cores(mod)

#a=np.linalg.norm(modcon)
#b=np.linalg.norm(mod.generation)
#const=a/b
#print(const)

#itmod=trTT.fixpoint_iteration_linear(mod,y,mu=const)
#itmod.set_points(mod.points)
#itmod.set_datacores("legendre")
#itmod.forward_generation()

#print(np.linalg.norm(mod.generation-y),np.linalg.norm(itmod.generation-y))


appmod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
appmod.set_points(mod.points)
appmod.set_datacores_from_input(mod.datacores)
appmod.init_coeffcores_random()

#trTT.iterative_hard_treshold_TT_general(appmod,y,nit=10,ntrit=100,trlength=100,factor=0.1)





## LINEAR TEST
lnit=0
model=mod
for i in range(lnit):
    nextit=trTT.fixpoint_iteration_linear(model,y,mu=7/(math.sqrt(ndat)),adjustmu=True)

    nextit.set_points(mod.points)
    nextit.set_datacores("legendre")
    nextit.forward_generation()

    print("Error after iteration",i)
    print(np.linalg.norm(nextit.generation-y), np.linalg.norm(y))

    treshit=trTT.treshold_TT_linear(nextit,mod,nit=100)

    treshit.set_points(mod.points)
    treshit.set_datacores("legendre")
    treshit.forward_generation()

    print("Error after tresholding",i)
    print(np.linalg.norm(treshit.generation-y), np.linalg.norm(y))

    model=treshit

#trTT.fixpoint_iteration_general(mod,y,1)

## COMPUTE AVERAGED METRIC CHANGE FROM COEFFICIENTS TO GENERATIONS
#geom_change=ha.geometry_change_parameter(mod,20,magnitude=1)
#print("Geometry change parameter:",geom_change)

#fac=np.linalg.norm(y)/(np.linalg.norm(ha.contract_cores(gen_mod)))
#trTT.iterative_hard_treshold_TT_linear(mod,y,nit=20,ntrit=10,factor=1/geom_change,genknown=True,genmod=gen_mod)

