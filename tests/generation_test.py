

import model_TT as moTT

import numpy as np
import helpers_nonlinear as hn
import helpers_core_optimization as hc

#########
#TEST PARAMETERS
# MANDy HAS ONLY MULTIPLICATIONS
#########
types=['mul','mul','mul','mul','mul']
#types=['plus','plus','plus']
nleg=3
ndim=4
ndat=5
ran=2
magn=1
lam=1

mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
mod.set_points_random()
mod.set_datacores_legendre()
mod.init_coeffcores_random()
mod.forward_generation(start=0,comgradients=True)



pos=0
Apos=np.copy(mod.coeffcores[pos])
phipos=np.copy(mod.datacores[pos])
Aposphipos=np.tensordot(Apos,phipos,axes=[2,0])


## SET FUNCTION AT POSMIN COMPUTE THE FORWARDED
disturbed=np.random.rand(ndat,ran)
delt_forward=hn.generation_step(disturbed,Apos,phipos,mod.types)

delt_check=np.zeros((ndat,ran))
for j in range(ndat):
    for l in range(ran):
        delt_check[j,l]=np.tensordot(disturbed[j,:],Aposphipos[:,l,ran],axes=([0,0]))



print(delt_forward)
print(delt_check)









