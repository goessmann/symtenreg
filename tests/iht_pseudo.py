import model_TT as moTT
import thresholding_TT as trTT

import numpy as np
import math

import helpers_nonlinear as hn
import helpers_analysis as ha


"""
Iterative Hard Tresholding for linear measurement operator A and X in the TT format: min(l2(AX-y)) subject to X low rank
    1) W^(j+1)=X^j + A^*[y-A(X^j)] -> fixpoint_iteration_linear()
    2) X^(j+1)=H_r(W^(j+1)) -> treshold_linear(): optimize low rank TT to approximate W
"""



#########
#TEST PARAMETERS
#########

nleg=4     ## Order of the Coefficient Tensor
ndim=2      ## Number of basisfunctions
ndat=10    ## Number of snapshots
ran=2       ## Rank of the Coefficient Tensor (TT)
magn=1      ## Magnitude of the random initialized Coefficients
types=["mul",'mul','mul','mul','mul','mul','mul','mul','plus','plus','plus']
outdim=4
size=20
nit=10
lam=1

## Initialization of the regressor
mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
mod.set_points_random()
mod.set_datacores("legendre")
mod.init_coeffcores_random(magnitude=magn)
mod.forward_generation(start=0,comgradients=True)

## Generation of test data
gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types,outdim)
gen_mod.set_points(mod.points)
gen_mod.set_datacores("legendre")
gen_mod.init_coeffcores_random(magnitude=magn)
y=gen_mod.forward_generation()


con_dat=trTT.contract_datacores(gen_mod)
inv=np.linalg.inv(con_dat)

gen_con=ha.contract_cores(gen_mod)

appmodel=mod
for i in range(nit):
    print("It",i)

    preres=y-appmodel.generation
    res=np.matmul(inv,preres)

    newmodel=trTT.fixpoint_iteration_linear(appmodel,y,com_res=False,residuum=res)
    newmodel.forward_generation()
    print("after it")
    print(np.linalg.norm(newmodel.generation-y)/np.linalg.norm(y))

    appmodel=trTT.treshold_TT_linear(newmodel,appmodel,nit=200)
    appmodel.set_datacores_from_input(mod.datacores)
    appmodel.forward_generation()



    print("after tres")
    print(np.linalg.norm(appmodel.generation-y)/np.linalg.norm(y))

    print("coeferror")
    app_con=ha.contract_cores(appmodel)

    print(np.linalg.norm(app_con-gen_con))
