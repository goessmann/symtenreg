

import model_TT as moTT

import numpy as np
import helpers_nonlinear as hn

#########
#TEST PARAMETERS
# MANDy HAS ONLY MULTIPLICATIONS
#########
types=['mul','mul','mul','mul','mul']
#types=['ReLU','plus','mul']
nleg=3
ndim=4
ndat=100
ran=2
magn=1

mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
mod.set_points_random()
mod.set_datacores_legendre()
mod.init_coeffcores_random(magnitude=magn)
mod.forward_generation(start=0,comgradients=True)

gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
gen_mod.set_points(mod.points)
gen_mod.set_datacores_legendre()
gen_mod.init_coeffcores_random(magnitude=magn)
y=gen_mod.forward_generation()

#mod.position_optimization(3,y,lam=0.1)
mod.backward_iteration(y,10,lam=1)
print(y[:10],mod.generation[:10])



## GRADIENT_F
pos=1
Apos=mod.coeffcores[pos,:,:,:]
phipos=mod.datacores[pos,:,:]
Aposphipos=np.tensordot(Apos,phipos,axes=([2,0]))
# HAS TO COINCIDE AND DOES
#print(mod.gradients_f[pos,1,:,:])
#print(Aposphipos[:,:,1])




## GRADIENT_A
pos=1
dat=2
l1=1
l2=1
ik=1
fold=mod.partfunctions[pos-1,dat,l1]
phipos=mod.datacores[pos,ik,dat]
# HAS TO COINCIDE AND DOES
#print(mod.gradients_A[pos,dat,l1,:,ik])
#print(fold*phipos)




## COEFFICIENT UPDATES

#print(mod.generation)
