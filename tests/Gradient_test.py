import model_TT as moTT

import numpy as np
import helpers_nonlinear as hn
import helpers_core_optimization as hc

#########
#TEST PARAMETERS
# MANDy HAS ONLY MULTIPLICATIONS
#########
types=['mul','mul','mul','mul','mul']
#types=['plus','plus','plus']
nleg=3
ndim=4
ndat=500
ran=2
magn=1
lam=1

mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
mod.set_points_random()
mod.set_datacores_legendre()
mod.init_coeffcores_random()
mod.forward_generation(start=0,comgradients=True)

gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
gen_mod.set_points(mod.points)
gen_mod.set_datacores_legendre()
gen_mod.init_coeffcores_random()
y=gen_mod.forward_generation()


#mod.backward_optimization(y,lam)












pos=0
cum_grad=mod.compute_cum_gradient_A(pos)
addminimum=hc.core_optimization_linearized(mod.cum_gradient,mod.generation,y,lam)

sh=cum_grad.shape
cum_grad=cum_grad.reshape(sh[0],sh[1]*sh[2]*sh[3])

addmin=addminimum.reshape(sh[1]*sh[2]*sh[3])
addsol=np.tensordot(cum_grad,addmin,axes=([1,0]))

gradient_A_pos=mod.gradients_A[pos]
grad_A_res=gradient_A_pos.reshape(sh[0],sh[1]*sh[2]*sh[3])



######
## TEST OF ONE GENERATION STEP
delt_f_pos=np.zeros((ndat,ran))
for j in range(ndat):
    for l in range(ran):

        graA=gradient_A_pos[j,:,l,:]
        addm=addminimum[:,l,:]
        sA=graA.shape
        g=graA.reshape(sA[0]*sA[1])
        a=addm.reshape(sA[0]*sA[1])
        delt_f_pos[j,l]=np.tensordot(g,a,axes=([0,0]))

old_f_pos=np.copy(mod.partfunctions[pos])
old_f_posplus=np.copy(mod.partfunctions[pos+1])

mod.coeffcores[pos,:,:,:]=mod.coeffcores[pos,:,:,:]+lam*addminimum
mod.forward_generation(0,comgradients=True)

new_f_pos=mod.partfunctions[pos]
should=old_f_pos+delt_f_pos

#Should be the same and are
#print(should[:3],new_f_pos[:3])
######











######
## TEST OF FORWARD GENERATION STEP AT POSPLUS
Ak=mod.coeffcores[pos+1]
phik=mod.datacores[pos+1]
delt=hn.generation_step(delt_f_pos,Ak,phik,mod.types)

new_f_posplus=np.copy(mod.partfunctions[pos+1])

#Should be the same and is
#print(old_f_posplus[:3]+delt[:3],new_f_posplus[:3])
#####





######
## TEST OF GENERATION STEP VIA GRADF
grad_f_posplus=mod.gradients_f[pos+1]
gdelt=np.zeros((ndat,ran))
for j in range(ndat):
    for l in range(ran):
        gdelt[j,l]=np.tensordot(delt_f_pos[j,:],grad_f_posplus[j,:,l],axes=([0,0]))

print(delt-gdelt)



#Needs to be the same as after optimization in hn and is
#print(np.linalg.norm(mod.generation+addsol-y))



print(pos,np.linalg.norm(y-mod.generation))

#mod.coeffcores[pos,:,:,:]=mod.coeffcores[pos,:,:,:]+lam*addminimum
#mod.forward_generation(pos)



#mod.position_optimization(0,y,lam)






#mod.backward_optimization(y,lam=0)
#mod.backward_iteration(y,50)
#print(y[:10],mod.generation[:10])

## GRADIENT_F
pos=1
Apos=mod.coeffcores[pos,:,:,:]
phipos=mod.datacores[pos,:,:]
Aposphipos=np.tensordot(Apos,phipos,axes=([2,0]))
# HAS TO COINCIDE AND DOES
#print(mod.gradients_f[pos,1,:,:])
#print(Aposphipos[:,:,1])

## GRADIENT_A
pos=1
dat=2
l1=1
l2=1
ik=1
fold=mod.partfunctions[pos-1,dat,l1]
phipos=mod.datacores[pos,ik,dat]
# HAS TO COINCIDE AND DOES
#print(mod.gradients_A[pos,dat,l1,:,ik])
#print(fold*phipos)




## COEFFICIENT UPDATES

#print(mod.generation)
