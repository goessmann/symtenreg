import helpers_nonlinear as hn
import helpers_core_optimization as hc

import numpy as np


class core():
    def __init__(self,position,lchild,rchild,parent,types,tensor):
        self.position=position
        self.left_child=lchild
        self.right_child=rchild
        self.parent=parent

        self.types=types
        self.tensor=tensor

        sh=tensor.shape
        self.inrank1=sh[0]
        self.inrank2=sh[1]
        self.outrank=sh[2]

    def reset_tensor(self,tensor):
        self.tensor=tensor

        sh=tensor.shape
        self.inrank1=sh[0]
        self.inrank2=sh[1]
        self.outrank=sh[2]

    def compute_function(self,infunk1,infunk2):
        """Input1,Input2: shape(ndat,ran1),(ndat,ran2)
        Need to set Tensor first!
        """
        ndat=len(infunk1)
        self.function=np.zeros((ndat,self.outrank))

        for l in range(self.outrank):
            for j in range(ndat):
                entry=0
                for l1 in range(self.inrank1):
                    for l2 in range(self.inrank2):
                        entry+=self.tensor[l1,l2,l]*hn.general_operation(infunk1[j,l1],infunk2[j,l2],self.types[l])
                self.function[j,l]=entry
        return self.function

    def compute_gradient1(self,infunk1,infunk2):
        ndat=len(infunk1)
        self.gradient1=np.zeros((ndat,self.inrank1,self.outrank))
        for j in range(ndat):
            for l1 in range(self.inrank1):
                for l in range(self.outrank):
                    co=0
                    for l2 in range(self.inrank2):
                        co+=hn.first_diff(infunk1[j,l1],infunk2[j,l2],self.types[l])*self.tensor[l1,l2,l]
                    self.gradient1[j,l1,l]=co
        return self.gradient1

    def compute_gradient2(self,infunk1,infunk2):
        ndat=len(infunk1)
        self.gradient2=np.zeros((ndat,self.inrank2,self.outrank))
        for j in range(ndat):
            for l2 in range(self.inrank2):
                for l in range(self.outrank):
                    co=0
                    for l1 in range(self.inrank1):
                        co+=hn.second_diff(infunk1[j,l1],infunk2[j,l2],self.types[l])*self.tensor[l1,l2,l]
                    self.gradient2[j,l2,l]=co
        return self.gradient2

    def compute_gradientA(self,infunk1,infunk2):
        ndat=len(infunk1)
        self.gradientA=np.zeros((ndat,self.inrank1,self.inrank2,self.outrank))
        for j in range(ndat):
            for l in range(self.outrank):
                for l1 in range(self.inrank1):
                    for l2 in range(self.inrank2):
                        self.gradientA[j,l1,l2,l]=hn.general_operation(infunk1[j,l1],infunk2[j,l2],self.types[l])
        return self.gradientA

class trans_matrix():
    def __init__(self,position,child,parent,matrix):
        self.position=position
        self.child=child
        self.parent=parent
        self.matrix=matrix

    def compute_function(self,infunk):
        self.function=np.tensordot(infunk,self.matrix,axes=[1,0])

    def compute_gradientA(self,infunk):
        self.gradientA=infunk

    def compute_gradient1(self):
        self.gradient1=self.matrix





class symbolic_model_HT():
    """ Parameters:
            nleg    Order of the coefficient tensor
            ran     TT-Rank of the coefficient tensor
            ndat    Number of snapshots
            ndim    Number of basisfunctions (Legendre polynomials by default)
            types   Operations in the generalized core contraction
            outdim  Output dimension of the model (=nleg for governing equations)

        Attributes:
            coeffcores      Coefficient tensor cores (as numpy arrays), except last
            outcore         Last coefficient tensor core
            neighbors       Parent, first child and second child of each coeffcore
            datacores       Datacores
            partfunctions   Generated function after partial contractions
            gradients_f     Gradients of the generated function wrt the previous function
            gradient_f_out
            gradients_A     Gradients of the generated function wrt the datacore
            gradient_A_out

            points          Snapshots
            generations     Resulting function after last contraction
            cum_gradient    Gradient of the generations wrt datacore
            loss            Frobenius distance of generated function and data
    """
    def __init__(self,nleg,ndat,ndim,types=['mul','plus','ReLU']):
        self.nleg=nleg
        self.ndat=ndat
        self.ndim=ndim
        self.types=types


        self.coeffcores=[]
        self.transmatrices=[]

        self.neighbores=np.zeros((len(self.coeffcores)+len(self.transmatrices),3))
        self.datacores=np.zeros((self.nleg,self.ndim,self.ndat))

        ## SHIFTED TO CORE COMPETENCE
        #self.ran=ran
        #self.outdim=outdim
        #self.coeffnumber=hn.ht_coef_number(self.nleg)
        #self.coeffcores=np.zeros((self.coeffnumber,self.ran,self.ran,self.ndim))
        #self.lowercores=np.zeros((int(self.nleg/2),self.ndim,self.ndim,self.ran))
        #self.outcore=np.zeros((self.ran,self.ran,self.outdim))
        #self.partfunctions=np.zeros((self.nleg,self.ndat,self.ran))
        #self.gradients_f=np.zeros((self.nleg,self.ndat,self.ran,self.ran))
        #self.gradients_A=np.zeros((self.nleg,self.ndat,self.ran,self.ran,self.ndim))

    def set_points_random(self):
        self.points=np.random.rand(self.ndat,self.nleg)

    def set_points(self,points):
        self.points=points

    def set_datacores(self,type):
        for k in range(self.nleg):
            self.datacores[k,:,:]=hn.datacore(self.points,self.ndim,k,type)

    def init_coeffcores(self,coeffcores,transmatrices,neighbores):
        self.coeffnumber=len(coeffcores)
        self.transnumber=len(transmatrices)
        self.neighbores=neighbores
        for k in range(self.coeffnumber):
            co=core(position=k,lchild=0,rchild=0,parent=0,types=self.types,tensor=coeffcores[k])
            self.coeffcores.append(co)
        for k in range(self.transnumber):
            tra=trans_matrix(k,child=0,parent=0,matrix=transmatrices[k])
            self.transmatrices.append(tra)



   # def init_coeffcores(self,lowercores,coeffcores,outcore):

#
 #       self.lowercores=lowercores
  #      self.coeffcores=coeffcores
   #     self.outcore=outcore
        #self.neighbores=hn.ht_neighbores(self.nleg)

    def init_coeffcores_random(self,magnitude=1):
        self.lowercores=np.random.rand(int(self.nleg/2),self.ndim,self.ndim,self.ran)
        self.coeffcores=np.random.rand(self.coeffnumber,self.ran,self.ran,self.ndim)
        self.outcore=np.random.rand(self.ran,self.ran,self.outdim)
        #self.neighbores=hn.ht_neighbores(self.nleg)


def init_binary_tree(lnegs):

