from __future__ import absolute_import, division, print_function, unicode_literals

import model_TT as moTT
import thresholding_TT as trTT
import helpers_core_optimization as hc
import helpers_nonlinear as hn

# Helper libraries
import numpy as np
import math
import matplotlib.pyplot as plt


class symbolic_model_CP():
    def __init__(self,nleg,ndat,ndim,ran=None,y=None,types=None,outdim=1,filter=None):
        self.nleg=nleg
        self.ndat=ndat
        self.ndim=ndim
        self.ran=ran


        self.types=types

        ## Not used!
        self.outdim=outdim

        self.y=y
        self.filter=np.identity(nleg)

        self.angle=19/100*np.pi

    def set_points(self,points):
        self.points=points

    def set_datacores(self,type="sincos",withoutone=True):
        self.ndat=len(self.points)

        self.datacores=np.zeros((self.nleg,self.ndim,self.ndat))
        self.datatype=type

        filteredpoints=np.tensordot(self.points,self.filter,axes=[1,1])

        for k in range(self.nleg):
            self.datacores[k,:,:]=hn.datacore(filteredpoints,self.ndim,k,type,withoutone)

    def set_datacores_from_input(self,datacores):
        ## Format: (self.nleg,self.ndim,self.ndat)
        self.datacores=datacores

    def set_datacores_gaussian(self):
        self.datacores=np.random.normal(0,1,(self.nleg,self.ndim,self.ndat))

    def set_coeffcores(self,coeffcores):
        ## Format: (self.nleg,self.outdim,self.ndim,self.ran)
        self.coeffcores=coeffcores

    def set_Z(self,Z):
        self.Z=Z




    def backward_iteration(self,y,size,strategy='ALS',reject=True,com_error=True,lam=0):

        """"
        Optimization:
            ALS: Assumes linear datacore dependencies
                ! Coeffcores by Datacores due to MANDy !
        Parameters:
            predpoints: Prediction points for the preddatacores
            y: Function to be approximated at prediction points
            strategy: type of optimization algorithm
            reject (to be implemented): rejection if not improved performance
            com_error: Error computation for control
        """
        if strategy=="ALS":
            trainsize=len(y)

            fulldatacores=np.copy(self.datacores)
            oldndat=self.ndat
            self.ndat=size

            for position in range(self.nleg-1,-1,-1):

                optset=np.random.choice(trainsize,size,replace=False)
                self.datacores=fulldatacores[:,:,optset]
                yopt=y[optset]

                ### OVERKILL, JUST NEED TO ADJUST CURRENT POSITION -> Could fasten here
                self.contract_coefdat()
                self.compute_stacks_coefdat()
                ###

                gradient=self.com_coeffcoregradient_function(position)

                for l in range(self.outdim):

                    operator=np.tensordot(gradient[:,l,:,:],gradient[:,l,:,:],axes=(0,0))
                    operator_reshaped=operator.reshape((self.ndim*self.ran,self.ndim*self.ran))


                    ### REGULARIZATION WITH PARAMETER LAMBDA
                    operator_reshaped=operator_reshaped+lam*np.identity(self.ndim*self.ran)

                    rhs=np.tensordot(gradient[:,l,:,:],yopt[:,l],axes=(0,0))
                    rhs_reshaped=rhs.reshape(self.ndim*self.ran)

                    coresolution=np.linalg.lstsq(operator_reshaped,rhs_reshaped,rcond=None)
                    update=coresolution[0].reshape(self.ndim,self.ran)

                    #print("leftstack",np.linalg.norm(self.leftstack_coefdat))
                    #print("gradientnorm",np.linalg.norm(gradient))
                    #print("rhsnorm",np.linalg.norm(rhs))
                    #print("Operatornorm",np.linalg.norm(operator),np.linalg.norm(operator_reshaped))

                    #########
                    if com_error:
                        print("Position",position)

                        gras=gradient.shape
                        gra_reshaped=gradient.reshape(gras[0],gras[1],gras[2]*gras[3])
                        oldcore_res=self.coeffcores[position,l].reshape(self.ndim*self.ran)

                        oldpred=np.tensordot(gra_reshaped[:,l,:],oldcore_res,axes=[1,0])
                        newpred=np.tensordot(gra_reshaped[:,l,:],coresolution[0],axes=[1,0])

                        error=np.linalg.norm(yopt[:,l]-newpred)

                        #print("olderror",np.linalg.norm(y[:,l]-oldpred))
                        print("prederror",error)

                        linpred=self.com_prediction_linear()
                        print(linpred.shape)

                        print(np.linalg.norm(yopt-linpred))
                    ########



                    self.coeffcores[position,l]=update



            self.datacores=fulldatacores
            self.ndat=oldndat

            if com_error:
                return error


####### CONSTRUCTION: BUILD STACKS COEFDAT AND COEFGRADIENT
    def contract_coefdat(self):
        ## CONTRACT COEFFICIENTCORES AND DATACORES PAIRWISE
        contracted=np.zeros((self.nleg,self.outdim,self.ran,self.ndat))
        for pos in range(self.nleg):
            for l in range(self.outdim):
                contracted[pos,l]=np.tensordot(self.coeffcores[pos,l],self.datacores[pos],axes=[0,0])
        self.contracted_coefdat=contracted

    def compute_stacks_coefdat(self):
        ## COMPUTE LEFTSTACK OF HADAMARD MULTIPLICATED CONTRACTED DATACORES
        leftstack=np.zeros((self.nleg,self.outdim,self.ran,self.ndat))
        leftstack[0]=np.ones((self.outdim,self.ran,self.ndat))
        for l in range(self.outdim):
            for pos in range(1,self.nleg):
                for j1 in range(self.ran):
                    for j2 in range(self.ndat):
                        leftstack[pos,l,j1,j2]=leftstack[pos-1,l,j1,j2]*self.contracted_coefdat[pos-1,l,j1,j2]

        ## COMPUTE RIGHTSTACK
        rightstack=np.zeros((self.nleg,self.outdim,self.ran,self.ndat))
        rightstack[self.nleg-1]=np.ones((self.outdim,self.ran,self.ndat))
        for l in range(self.outdim):
            for pos in range(self.nleg-2,-1,-1):
                for j1 in range(self.ran):
                    for j2 in range(self.ndat):
                        rightstack[pos,l,j1,j2]=rightstack[pos+1,l,j1,j2]*self.contracted_coefdat[pos+1,l,j1,j2]
        self.leftstack_coefdat=leftstack
        self.rightstack_coefdat=rightstack

    def com_coeffcoregradient_function(self,pos):
        gradient=np.zeros((self.ndat,self.outdim,self.ndim,self.ran))

        ## PROBABLY MORE EFFECTIVE WITH EINSUM?
        for l in range(self.outdim):
            for j in range(self.ndat):
                for k in range(self.ran):
                    gradient[j,l,:,k]=self.leftstack_coefdat[pos,l,k,j]*self.rightstack_coefdat[pos,l,k,j]*self.datacores[pos,:,j]
        return gradient

    def com_prediction_linear(self):
        self.contract_coefdat()

        prepred=np.ones((self.ndat,self.outdim,self.ran))
        for pos in range(self.nleg):
            for j in range(self.ndat):
                for l in range(self.outdim):
                    for k in range(self.ran):
                        prepred[j,l,k]=prepred[j,l,k]*self.contracted_coefdat[pos,l,k,j]

        predictions=np.zeros((self.ndat,self.outdim))
        for j in range(self.ndat):
            for l in range(self.outdim):
                predictions[j,l]=np.sum(prepred[j,l,:])

        self.predictions_linear=predictions
        return self.predictions_linear


    def com_prediction_alternative(self,orthcoefcores,typ="linear"):
        oldcores=np.copy(self.coeffcores)
        self.coeffcores=orthcoefcores

        if typ=="linear":
            self.predictions_alternative=self.com_prediction_linear()
        elif typ=="types":
            self.predictions_alternative=self.com_prediction_types()

        self.coeffcores=oldcores
        return self.predictions_alternative


    def com_prediction_types(self):

        ## Contract Datacores and Coeffcores
        contracted=np.zeros((self.nleg,self.outdim,self.ndat,self.ran))
        for l in range(self.outdim):
            for pos in range(self.nleg):
                contracted[pos,l]=np.tensordot(self.datacores[pos],self.coeffcores[pos,l],axes=[0,0])

        ## Iterative through position compute the forward pass
        partfunctions=np.zeros((self.nleg,self.outdim,self.ndat,self.ran))
        partfunctions[0]=contracted[0]

        for l in range(self.outdim):
            for pos in range(1,self.nleg):
                for j in range(self.ndat):
                    for k in range(self.ran):
                        partfunctions[pos,l,j,k]=hn.general_operation(partfunctions[pos-1,l,j,k],contracted[pos,l,j,k],self.types[pos,k])

        self.predictions_types=np.zeros((self.ndat,self.outdim))
        for l in range(self.outdim):
            for j in range(self.ndat):
                self.predictions_types[j,l]=np.sum(partfunctions[self.nleg-1,l,j])

        return self.predictions_types

    def lin_types_correlation(self):
        linpred=self.com_prediction_linear()
        typespred=self.com_prediction_types()
        cor=0
        n1=0
        n2=0
        for l in range(self.outdim):
            for j in range(self.ndat):
                cor+=linpred[j,l]*typespred[j,l]
                n1+=linpred[j,l]**2
                n2+=typespred[j,l]**2
        cor=cor/(math.sqrt(n1)*math.sqrt(n2))
        return cor

    def alt_lin_correlation(self,altcoeffcores):
        linpred=self.com_prediction_linear()
        altpred=self.com_prediction_alternative(altcoeffcores)
        cor=0
        n1=0
        n2=0
        for l in range(self.outdim):
            for j in range(self.ndat):
                cor+=linpred[j,l]*altpred[j,l]
                n1+=linpred[j,l]**2
                n2+=altpred[j,l]**2
        cor=cor/(math.sqrt(n1)*math.sqrt(n2))
        return cor




############ FILTEROPTIMIZATION ###################


    ## Assumes a MANDy kernel structure: Coeffcores by Datacores contracted with Z
    def com_datacoregradient_function(self,pos):
        try:
            preddatacores=self.preddatacores
            self.preddat=len(preddatacores[0,0])

        except:
            self.preddatacores=np.copy(self.datacores)
            self.preddat=self.ndat
            print("Preddatacores not set")

        gradient=np.zeros((self.preddat,self.outdim,self.ndim,self.ndat))
        pregrad=np.zeros((self.preddat,self.ndat,self.ndim))

        for j2 in range(self.preddat):
            for j1 in range(self.ndat):
                pregrad[j2,j1,:]=self.leftstack[pos,j1,j2]*self.rightstack[pos,j1,j2]*self.preddatacores[pos,:,j1]

        if len(self.Z.shape)==2:
            for l in range(self.outdim):
                for j in range(self.ndat):
                    gradient[:,l,:,j]=self.Z[j,l]*pregrad[:,j,:]
            #print("shape 2")

        if len(self.Z.shape)==1:
            for j in range(self.ndat):
                gradient[:,0,:,j]=self.Z[j]*pregrad[:,j,:]

            #print("Pregradnorm",np.linalg.norm(pregrad))
            #print("Znorm",np.linalg.norm(self.Z))
            #print("Lstacknorm",np.linalg.norm(self.leftstack[2]))
            #print("Rstacknorm",np.linalg.norm(self.rightstack[1]))
            #print("Contractednorm",np.linalg.norm(self.contracted))
            #print("Datacorenorm",np.linalg.norm(self.datacores))
            #print("predDatacorenorm",np.linalg.norm(self.preddatacores))
            #print("shape 1")

        return gradient




    def set_filter(self,A):
        self.filter=A

    def compute_gram(self):
        self.gram=hc.contract_datacores(self)

    def compute_Z(self,y,com_error=True):
        self.compute_gram()
        self.Z=np.linalg.lstsq(self.gram,y,rcond=None)[0]

        if com_error:
            pred=np.tensordot(self.gram,self.Z,axes=[0,0])
            print("MANDyerror",np.linalg.norm(pred-y))

    def com_prediction_kernel(self,predpoints,type="sincos"):
        self.datatype=type

        ## SET DATACORES FOR TEST POINTS
        preddat=len(predpoints)
        self.preddat=preddat

        filteredpoints=np.tensordot(self.filter,predpoints,axes=[1,1]).transpose()
        preddatacores=np.zeros((self.nleg,self.ndim,preddat))

        for k in range(self.nleg):
            for j in range(preddat):
                if self.datatype=="sincos":
                    preddatacores[k,0,j]=np.sin(filteredpoints[j,k]*self.angle)
                    preddatacores[k,1,j]=np.cos(filteredpoints[j,k]*self.angle)

        self.preddatacores=preddatacores

        contracted=np.zeros((self.nleg,self.ndat,preddat))
        predgram=np.zeros((self.ndat,preddat))

        for pos in range(self.nleg):
            contracted[pos]=np.tensordot(self.datacores[pos],preddatacores[pos],axes=[0,0])

        self.contracted=contracted

        ## COMPUTE GRAMMATRIX BETWEEN TRAINING AND TEST POINTS
        for j1 in range(self.ndat):
            for j2 in range(preddat):
                entry=1
                for pos in range(self.nleg):
                    entry=entry*contracted[pos,j1,j2]
                predgram[j1,j2]=entry

        #print(self.preddat,self.ndat,self.Z.shape)

        self.predictions=np.tensordot(predgram,self.Z,axes=[0,0])

    def compute_stacks_datpredat(self):

        try:
            preddatacores=self.preddatacores
            self.preddat=len(preddatacores[0,0])

        except:
            self.preddatacores=np.copy(self.datacores)
            self.preddat=self.ndat
            print("Preddatacores set in Stacks")


        try:
            contracted=self.contracted
        except:
            contracted=np.zeros((self.nleg,self.ndat,self.preddat))
            predgram=np.zeros((self.ndat,self.preddat))
            for pos in range(self.nleg):
                contracted[pos]=np.tensordot(self.datacores[pos],self.preddatacores[pos],axes=[0,0])

            self.contracted=contracted

        ## COMPUTE LEFTSTACK OF HADAMARD MULTIPLICATED CONTRACTED DATACORES
        leftstack=np.zeros((self.nleg,self.ndat,self.preddat))
        leftstack[0]=np.ones((self.ndat,self.preddat))
        for pos in range(1,self.nleg):
            for j1 in range(self.ndat):
                for j2 in range(self.preddat):
                    leftstack[pos,j1,j2]=leftstack[pos-1,j1,j2]*self.contracted[pos-1,j1,j2]

        ## COMPUTE RIGHTSTACK
        rightstack=np.zeros((self.nleg,self.ndat,self.preddat))
        rightstack[self.nleg-1]=np.ones((self.ndat,self.preddat))
        for pos in range(self.nleg-2,-1,-1):
            for j1 in range(self.ndat):
                for j2 in range(self.preddat):
                    rightstack[pos,j1,j2]=rightstack[pos+1,j1,j2]*self.contracted[pos+1,j1,j2]
        self.leftstack=leftstack
        self.rightstack=rightstack


    ## GD Optimization of the filter position, Before need to compute stacks (one time for all positions)
    def com_datacoregradient_loss(self,pos,residuum):

        ## CONTRACT Z AND RESIDUUM
        Zres=np.tensordot(self.Z,residuum,axes=[1,1])

        ## COMPUTE THE WHOLE ABOVE OPERATOR USING LEFT AND RIGHTSTACK WITH THE TRAININGS DATACORE
        aboveop=np.zeros((self.preddat,self.ndim))
        for j2 in range(self.preddat):
            entry=np.zeros(self.ndim)
            for j1 in range(self.ndat):
                entry=entry+self.leftstack[pos,j1,j2]*self.rightstack[pos,j1,j2]*Zres[j1,j2]*self.datacores[pos,:,j1]
            aboveop[j2,:]=entry

        return aboveop

    def optimize_filter_position(self,pos,predpoints,residuum,length=0.01,windowsize=3,pixellength=28):
        window=pixelwindow(pos,windowsize=windowsize,length=pixellength)
        aboveop=self.com_datacoregradient_loss(pos,residuum)

        ## COMPUTE TEST DATACORE GRADIENT WRT FILTER
        deltaphi=np.zeros((self.ndim,self.preddat,self.nleg))
        for j in range(self.preddat):
            lastAx=np.dot(self.filter[pos],predpoints[j])
            for l in window:
            ############
            #### COULD REPLACE WITH MORE GENERAL FEATURE MAP AND REDUCE FILTER WIDTH
            ############
                deltaphi[0,j,l]=-np.cos(self.angle*lastAx)*self.angle*predpoints[j,l]
                deltaphi[1,j,l]=np.sin(self.angle*lastAx)*self.angle*predpoints[j,l]


        gradient_filter=np.tensordot(deltaphi,aboveop,axes=([0,1],[1,0]))
        self.filter[pos]=self.filter[pos]+length*gradient_filter

        #print("Updatenorm position",pos,np.linalg.norm(update))


    def filter_optimization(self,predpoints,y,length=0.01,wsize=3,plength=28):
        oldfilter=np.copy(self.filter)

        self.preddat=len(predpoints)

        self.com_prediction_kernel(predpoints)

        self.compute_stacks_datpredat()

        residuum=y-self.prediction

        for pos in range(self.nleg):
            self.optimize_filter_position(pos,predpoints,residuum,length,windowsize=wsize,pixellength=plength)

        print("Filterupdate:",np.linalg.norm(self.filter-oldfilter))



    def iterative_MANDY_filteropt(self,trainingpoints,y,nit,length=1e-80,wsize=1,plength=28,MANDysize=8,filtersize=7,filternit=3):
        errorhistory=[]
        filterhistory=[]

        trainsize=len(trainingpoints)

        for it in range(nit):
            MANDychoice=np.random.choice(trainsize,MANDysize,replace=False)

            print("Iteration",it)

            ## MANDy computation
            self.ndat=MANDysize
            self.set_points(trainingpoints[MANDychoice])
            print(self.points.shape)

            self.set_datacores()
            self.compute_Z(y[MANDychoice])

            for fit in range(filternit):
            ## Filter update
                filterchoice=np.random.choice(trainsize,filtersize,replace=False)
                self.filter_optimization(trainingpoints[filterchoice],y[filterchoice],length,wsize,plength)

            #print(self.prediction[:2]-y[:2])
            filtererror=np.linalg.norm(self.prediction-y[filterchoice])

            print("Filternorm",np.linalg.norm(self.filter))
            print("ErroronFilterdata",np.linalg.norm(self.prediction-y[filterchoice]))

            filterhistory.append(filtererror)

        return filterhistory











#### FILTER BUILDINGS

def pixelrow(pos,length=28):
    return int(np.floor(pos/length))

def pixelpos(pos,length=28):
    pixelrow=int(np.floor(pos/length))
    pixelcolumn=int(pos-length*pixelrow)
    return pixelcolumn,pixelrow

def vectorpos(pixelcolumn,pixelrow,length=28):
    return pixelcolumn+pixelrow*length

def pixelwindow(pos,windowsize=3,length=28):
    pixelcolumn,pixelrow=pixelpos(pos,length)
    window=[]
    for i in range(-windowsize,windowsize+1):
        for j in range(-windowsize,windowsize+1):
            if pixelcolumn+i>-1 and pixelrow+j>-1:
                if pixelcolumn+i<length and pixelrow+j<length:
                    window.append(vectorpos(pixelcolumn+i,pixelrow+j,length))
    return window


