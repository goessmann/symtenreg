import numpy as np
import model_TT as moTT
import helpers_nonlinear as hn

import scipy as sp
from scipy.optimize import minimize
from scipy.optimize import LinearConstraint

##################################
### Tensor Train Optimization: ALS
##################################

def contract_fy(grad,func,y):
    s=func.shape
    ndat=s[0]
    outdim=s[1]
    sh=grad.shape
    con_fy=np.zeros((sh[1],sh[2],sh[3]))
    for j in range(ndat):
        for l in range(outdim):
            con_fy=con_fy+grad[j,:,:,:,l]*(y[j,l]-func[j,l])
    return con_fy

def newform(gradient):
    ndat=gradient.shape[0]
    ranold=gradient.shape[1]
    rannew=gradient.shape[2]
    ndim=gradient.shape[3]
    outdim=gradient.shape[4]
    grad_newform=np.zeros((ndat,outdim,ranold,rannew,ndim))
    for j in range(ndat):
        for l in range(outdim):
            grad_newform[j,l]=gradient[j,:,:,:,l]
    grad_re=grad_newform.reshape(ndat*outdim,ranold,rannew,ndim)
    return grad_re

# ALS OPTIMIZATION OF A COEFFICIENT CORE: DEFINE AND SOLVE THE LINEAR EQUATION
def core_optimization_linearized(grad,func,y,lamb=0):

    rhs=contract_fy(grad,func,y)
    ##########################################
    ## CHECK CONTRACT_FY
    ## BUILD THE RIGHT HAND SIGHT CONSTANT IN CASE OF ONE OUTDIM
    #contracted_f=np.tensordot(grad,func,axes=[0,0])
    #contracted_y=np.tensordot(grad,y,axes=[0,0])
    #rhs2=contracted_y-contracted_f
    # SHOULD BE THE SAME AND IS
    #print(rhs2,rhs)
    ##########################################

    ## BUILD THE LEFT HAND SIDE OPERATOR
    grad_re=newform(grad)
    op=np.tensordot(grad_re,grad_re,axes=[0,0])
    ##########################################
    ## CHECK OPERATOR
    # SHOULD BE THE SAME IN CASE OUTDIM=1 AND IS
    #op2=np.tensordot(grad,grad,axes=[0,0])
    #print('Op old op new')
    #print(op2[0,0,0],op[0,0,0])
    ##########################################

    ranold=op.shape[0]
    rannew=op.shape[1]
    ndim=op.shape[2]
    op=op.reshape(ranold*rannew*ndim,ranold*rannew*ndim)

    ## FIND THE LEAST SQUARES SOLUTION
    #! HYPERPARAMETER FOR REGULARIZATION: LAMB
    op_reg=op+np.ones((ranold*rannew*ndim,ranold*rannew*ndim))*lamb
    rhs=rhs.reshape(ranold*rannew*ndim)
    addminimum=np.linalg.lstsq(op_reg,rhs,rcond=None)
    addmin=addminimum[0].reshape((ranold,rannew,ndim))

    ##########################################
    ## CHECK FOUND MINIMUM
    ## LOSS REDUCTION IN THE LINEARIZED THEORY
    ## SHOULD SAME AS AFTER GENERATION (CHECKBLOCK IN _ALS)
    #s=grad.shape
    #gra=grad.reshape(s[0],s[1]*s[2]*s[3],s[4])
    #print('Gradientshape',gra.shape)
    #addsol=np.tensordot(addminimum[0],gra,axes=([0,1]))
    #modified_solution=np.zeros(func.shape)
    #for j in range(len(func)):
    #    for l in range(len(func[0])):
    #        modified_solution[j,l]=func[j,l]+addsol[j,l]
    #print('OPTIMIZATION LOSS REDUCED:')
    #print(loss(func,y),loss(modified_solution,y))
    #print('FIRST GENERATIONS OPTIMIZED:')
    #print(modified_solution[:1])
    ##########################################

    return addmin

# ALS step with additional rejection possibility (used in nonlinear theory)
def position_optimization_ALS(mod,pos,y,lam,forward=True,comgradients=True,rejection=False):
    prior_loss=loss(mod.generation,y)

    ## UPDATE THE CORE
    gradient_cum=np.copy(mod.compute_cum_gradient_A(pos))
    addminimum=core_optimization_linearized(gradient_cum,mod.generation,y)
    #print(pos,addminimum.shape)

    if pos==mod.nleg-1:
        mod.outcore=np.copy(mod.outcore+lam*addminimum)
    else:
        mod.coeffcores[pos,:,:,:]=np.copy(mod.coeffcores[pos,:,:,:]+lam*addminimum)

    ## FORWARD GENERATION TO READJUST THE PARTFUNCTIONS AND GRADIENTS
    if forward:
        mod.forward_generation(pos,comgradients)

        ##########################################
        ## CHECK
        #print('MATCH WITH GENERATION?')
        #print(mod.generation[:1])
        ##########################################

        if rejection:
            posterior_loss=loss(mod.generation,y)

            ##########################################
            ## CHECK: MATCH WITH POSTERIOR LOSS IN LINEARIZED CORE OPTIMIZATION?
            #print('posterior loss',posterior_loss)
            ##########################################

            if posterior_loss>prior_loss:
                if pos==mod.nleg-1:
                    mod.outcore=np.copy(mod.outcore-lam*addminimum)
                else:
                    mod.coeffcores[pos,:,:,:]=np.copy(mod.coeffcores[pos,:,:,:]-lam*addminimum)

                print('Position',pos,'did not get better')
                mod.forward_generation(pos,comgradients)

        ##########################################
        ## CHECK
        ## PRINT THE GENERATED FUNCTION
        #print('NEW GEN AFTER FORWARD GENERATION')
        #print(self.generation[:10])
        #print(pos,np.linalg.norm(y-self.generation))
        ##########################################


##################################
### Tensor Train Optimization: GD
##################################

def position_optimization_GD(mod,pos,y,lam,data,forward=True,comgradients=True,rejection=True):
    prior_loss=loss(mod.generation,y)

    ## COMPUTE THE GRADIENT OF THE LOSS WRT THE CORE
    gradient_cum=np.copy(mod.compute_cum_gradient_A(pos))
    gradient_L=-2*contract_fy(gradient_cum,mod.generation,y)


    ## UPDATE THE CORE, STEPWIDTH PARAMETER lam DIVIDED BY DATASIZE
    if pos==mod.nleg-1:
        mod.outcore=np.copy(mod.outcore-(lam/len(data))*gradient_L)
    else:
        mod.coeffcores[pos,:,:,:]=np.copy(mod.coeffcores[pos,:,:,:]-(lam/len(data))*gradient_L)

    ## FORWARD GENERATION TO READJUST THE PARTFUNCTIONS AND GRADIENTS
    if forward:
        mod.forward_generation(pos,comgradients)
        ## REJECTING THE UPDATE IF LOSS INCREASES
        if rejection:
            posterior_loss=loss(mod.generation,y)
            if posterior_loss>prior_loss:
                if pos==mod.nleg-1:
                    mod.outcore=np.copy(mod.outcore+(lam/len(data))*gradient_L)
                else:
                    mod.coeffcores[pos,:,:,:]=np.copy(mod.coeffcores[pos,:,:,:]+(lam/len(data))*gradient_L)
                print('Position',pos,'did not get better')
                mod.forward_generation(pos,comgradients)



# FROBENIUS LOSS BETWEEN PREDICTION AND DATA
def loss(func,y):
    return np.linalg.norm(func-y)

# VALUE TO BE SET TO ZERO IN THE OPTIMIZATION
def local_update_loss(grad,func,y):
    contracted_f=np.tensordot(grad,func,axes=[0,0])
    contracted_y=np.tensordot(grad,y,axes=[0,0])
    contracted_diff=contracted_f-contracted_y
    return np.linalg.norm(contracted_diff)

################################################################
### Tensor Train Optimization: Update with Pseudoinverse (MANDy)
################################################################

"""Compute the l2-minimizing Coeffcores in one sweep"""
def set_min_solution(mod,y):
    Z=compute_Z(mod,y)

    ran=mod.outdim*mod.ndat
    types=["mul" for l in range(ran)]
    outmod=moTT.symbolic_model_TT(mod.nleg,mod.ndat,mod.ndim,ran,types,mod.outdim)

    ##First Coeffcore
    firstcore=np.zeros((outmod.ran,mod.ndim))
    for j in range(mod.ndat):
        for l in range(mod.outdim):
            pos=j*mod.outdim+l
            firstcore[pos,:]=mod.datacores[0,:,j]
    outmod.coeffcores[0,0]=firstcore

    ##Middle Coeffcores
    for k in range(1,mod.nleg-1):
        core=np.zeros((outmod.ran,outmod.ran,mod.ndim))
        for j in range(mod.ndat):
            for l in range(mod.outdim):
                pos=j*mod.outdim+l
                core[pos,pos,:]=mod.datacores[k,:,j]
        outmod.coeffcores[k]=core

    ##Last Coeffcore
    lastcore=np.zeros((outmod.ran,mod.outdim,mod.ndim))
    if len(Z.shape)==1:
        for j in range(mod.ndat):
                lastcore[j,0,:]=mod.datacores[mod.nleg-1,:,j]*Z[j]
    else:
        for j in range(mod.ndat):
            for l in range(mod.outdim):
                pos=j*mod.outdim+l
                lastcore[pos,l,:]=mod.datacores[mod.nleg-1,:,j]*Z[j,l]

    outmod.outcore=lastcore
    outmod.set_points(mod.points)
    outmod.set_datacores_from_input(mod.datacores)
    return outmod


########################################################################################
### Tensor Train Optimization: Update the Filter transforming Data before the coeffcores
########################################################################################

"""Optimize the filters A transforming the data before the feature map"""
"""Built on TT storage of the coefficienttensor -> suboptimal!"""
def filter_optimization(mod,A,residuum,length=0.01):
    newA=np.copy(A)

    phiAcontractions=np.zeros((mod.nleg-1,mod.ran,mod.ran,mod.ndat))
    for pos in range(mod.nleg-2,-1,-1):
        phiAcontractions[pos]=np.tensordot(mod.coeffcores[pos],mod.datacores[pos],axes=[2,0])

## Generate left blocks
    leftblocks=np.zeros((mod.nleg-1,mod.ndat,mod.ran))
    leftblocks[0]=np.tensordot(mod.datacores[0],mod.coeffcores[0,0],axes=[0,1])
    for pos in range(mod.nleg-2):
        for j in range(mod.ndat):
            leftblocks[pos+1,j]=np.tensordot(leftblocks[pos,j],phiAcontractions[pos+1,:,:,j],axes=[0,0])

## Compute right blocks
    rightblocks=np.zeros((mod.nleg-1,mod.ndat,mod.ran,mod.outdim))
    rightblocks[mod.nleg-2]=np.tensordot(mod.datacores[mod.nleg-1],mod.outcore,axes=[0,2])
    for pos in range(mod.nleg-3,-1,-1):
        for j in range(mod.ndat):
            rightblocks[pos,j]=np.tensordot(phiAcontractions[pos+1,:,:,j],rightblocks[pos+1,j],axes=[1,0])

## Compute Gradient and update
    for pos in range(mod.nleg-1,-1,-1):
        ## LAST POSITION
        if pos==mod.nleg-1:
            contracted=np.tensordot(leftblocks[mod.nleg-2],mod.outcore,axes=[1,0])
        ## FIRST POSITION
        elif pos==0:
            contracted=np.tensordot(rightblocks[0],mod.coeffcores[0,0],axes=[1,0])
        ## MIDDLE POSITION
        else:
            precontracted=np.tensordot(leftblocks[pos-1],mod.coeffcores[pos],axes=[1,0])
            contracted=np.zeros((mod.ndat,mod.outdim,mod.ndim))
            for j in range(mod.ndat):
                contracted[j]=np.tensordot(rightblocks[pos,j],precontracted[j],axes=[0,0])

        deltaphi=np.zeros((mod.ndim,mod.ndat,mod.nleg))
        for j in range(mod.ndat):
            lastAx=np.dot(A[pos],mod.points[j])
            for l in range(mod.nleg):
            ############
            #### COULD REPLACE WITH MORE GENERAL FEATURE MAP
            ############
                deltaphi[0,j,l]=-np.cos(lastAx)*mod.points[j,l]
                deltaphi[1,j,l]=np.sin(lastAx)*mod.points[j,l]

        update=np.zeros((mod.nleg,mod.nleg))
        for j in range(mod.ndat):
            preupdate=np.zeros(mod.ndim)
            for l in range(mod.outdim):
                preupdate+=contracted[j,l]*residuum[j,l]
            update+=np.tensordot(preupdate,deltaphi[:,j,:],axes=[0,0])

        newA[pos]=newA[pos]+length*update
    return newA


########################################################################
### CP Optimization: Update Filter by GD and Coeffcores by Pseudoinverse
########################################################################

def contract_datacores(mod):
    contracted=np.zeros((mod.nleg,mod.ndat,mod.ndat))
    matrix=np.zeros((mod.ndat,mod.ndat))
    for pos in range(mod.nleg):
        contracted[pos]=np.tensordot(mod.datacores[pos],mod.datacores[pos],axes=[0,0])
    for j1 in range(mod.ndat):
        for j2 in range(mod.ndat):
            entry=1
            for pos in range(mod.nleg):
                entry=entry*contracted[pos,j1,j2]
            matrix[j1,j2]=entry
    return matrix

def compute_Z(mod,y):
    gramm=contract_datacores(mod)
    return np.linalg.solve(gramm,y)

def forward_generation_linear(mod):
    phiAcontractions=np.zeros((mod.nleg-1,mod.ran,mod.ran,mod.ndat))
    for pos in range(mod.nleg-2,-1,-1):
        phiAcontractions[pos]=np.tensordot(mod.coeffcores[pos],mod.datacores[pos],axes=[2,0])
    phiAout=np.tensordot(mod.outcore,mod.datacores[mod.nleg-1],axes=[2,0])

    oldfunction=phiAcontractions[0,0].transpose()
    for pos in range(mod.nleg-1):
        newfunction=np.zeros((mod.ndat,mod.ran))
        for j in range(mod.ndat):
            newfunction[j]=np.tensordot(oldfunction[j],phiAcontractions[pos,:,:,j],axes=[0,0])
        oldfunction=np.copy(newfunction)

    finalfunction=np.zeros((mod.ndat,mod.outdim))
    for j in range(mod.ndat):
        finalfunction[j]=np.tensordot(oldfunction[j],phiAout[:,:,j],axes=[0,0])
    return finalfunction


def iterative_MANDY_filter(mod,y,nit,initA,trainingpoints,step=1e20,com_filtererror=True):
    for it in range(nit):
    ## MANDY UPDATE
        mod=set_min_solution(mod,y)

        lingenerated=forward_generation_linear(mod)
        residuum=y-lingenerated

        print("IT "+str(it)+" MANDY error "+str(np.linalg.norm(residuum)))

    ## FILTER UPDATE
        newA=filter_optimization(mod,initA,residuum,length=step)
        newpoints=np.tensordot(newA,trainingpoints,axes=[1,1]).transpose()

        mod.set_points(newpoints)
        mod.set_datacores("sincos")

        if com_filtererror:
            lingeneratednew=forward_generation_linear(mod)
            residuum=y-lingeneratednew

            print("FILTER error"+str(np.linalg.norm(residuum)))

    return mod


########################
### CP Optimization: ALS
########################

#Implementation just for the linear case (all we need in index models)


