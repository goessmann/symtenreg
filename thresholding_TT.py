import model_TT as moTT

import helpers_nonlinear as hn
import helpers_analysis as ha
import helpers_core_optimization as hc

import numpy as np
import math

#########################
## LINEAR CONTRACTIONS
#########################

"""
Iterative Hard Tresholding for linear measurement operator A and X in the TT format: min(l2(AX-y)) subject to X low rank
    1) W^(j+1)=X^j + A^*[y-A(X^j)] -> fixpoint_iteration_linear()
    2) X^(j+1)=H_r(W^(j+1)) -> treshold_linear(): optimize low rank TT to approximate W
"""

"""
Alternative Update Operator by the Moore Penrose Pseudoinverse -> MANDy inspired
"""

def contract_datacores(mod):
    contracted=np.zeros((mod.nleg,mod.ndat,mod.ndat))
    matrix=np.zeros((mod.ndat,mod.ndat))
    for pos in range(mod.nleg):
        contracted[pos]=np.tensordot(mod.datacores[pos],mod.datacores[pos],axes=[0,0])
    for j1 in range(mod.ndat):
        for j2 in range(mod.ndat):
            entry=1
            for pos in range(mod.nleg):
                entry=entry*contracted[pos,j1,j2]
            matrix[j1,j2]=entry
    return matrix

def fixpoint_iteration_linear(mod,y,mu=1,adjustmu=False,com_res=True,residuum=None):
    if com_res:
        mod.forward_generation(start=0,comgradients=False)
        residuum=y-mod.generation

    ## Construct addmod, to be added to the mod (A^*[y-AX])
    types=["mul" for r in range(mod.ndat*mod.outdim)]
    addmod=moTT.symbolic_model_TT(mod.nleg,mod.ndat,mod.ndim,mod.ndat*mod.outdim,types=types,outdim=mod.outdim)

    ##First Coeffcore
    firstcore=np.zeros((addmod.ran,addmod.ndim))
    for j in range(mod.ndat):
        for l in range(mod.outdim):
            pos=j*mod.outdim+l
            firstcore[pos,:]=mod.datacores[0,:,j]
    addmod.coeffcores[0,0]=firstcore

    ##Middle Coeffcores
    for k in range(1,mod.nleg-1):
        core=np.zeros((addmod.ran,addmod.ran,addmod.ndim))
        for j in range(mod.ndat):
            for l in range(mod.outdim):
                pos=j*mod.outdim+l
                core[pos,pos,:]=mod.datacores[k,:,j]
        addmod.coeffcores[k]=core

    ##Last Coeffcore
    lastcore=np.zeros((addmod.ran,addmod.outdim,addmod.ndim))
    for j in range(mod.ndat):
        for l in range(mod.outdim):
            pos=j*mod.outdim+l
            lastcore[pos,l,:]=mod.datacores[mod.nleg-1,:,j]*residuum[j,l]
    addmod.outcore=lastcore

    if adjustmu:
        addmod.set_points(mod.points)
        addmod.set_datacores_from_input(mod.datacores)
        addmod.forward_generation(comgradients=False)
        gen_norm=np.linalg.norm(addmod.generation)
        addmod_con=ha.contract_cores(addmod)
        up_norm=np.linalg.norm(addmod_con)
        mu=gen_norm/up_norm

    ## Construct X+A^*[y-AX] as newmod
    newtypes=mod.types[:mod.ran]+["mul" for r in range(mod.ndat*mod.outdim)]
    newmod=moTT.symbolic_model_TT(mod.nleg,mod.ndat,mod.ndim,mod.ran+mod.ndat*mod.outdim,newtypes,outdim=mod.outdim)
    newmod.coeffcores[:,:mod.ran,:mod.ran,:]=mod.coeffcores
    newmod.coeffcores[1:,mod.ran:,mod.ran:,:]=mu*addmod.coeffcores[1:]
    newmod.coeffcores[0,0,mod.ran:,:]=mu*addmod.coeffcores[0,0]
    newmod.outcore[:mod.ran]=mod.outcore
    newmod.outcore[mod.ran:]=mu*addmod.outcore

    newmod.set_datacores_from_input(mod.datacores)
    return newmod


def position_optimization_tresholding_linear(mod,appmod,position):
    ## SOLVE OPTIMUM CONDITION FIRST ORDER:
    #   <appmod,grad_appmod>=<mod,grad_appmod>
    #    newcore*operator=rhs

    ## CONTRACT BEGINNING IN THE TOP
    if position!=0:
        obenrechts=np.ones((mod.ran,appmod.ran))
        obenlinks=np.ones((appmod.ran,appmod.ran))
    else:
        obenrechts=np.zeros((mod.ran,appmod.ran))
        obenrechts[0,0]=1
        obenlinks=np.zeros((appmod.ran,appmod.ran))
        obenlinks[0,0]=1

    for pos in range(position):
        preor=np.tensordot(obenrechts,mod.coeffcores[pos],axes=[0,0])
        obenrechts=contract0022(preor,appmod.coeffcores[pos])

        preol=np.tensordot(obenlinks,appmod.coeffcores[pos],axes=[0,0])
        obenlinks=contract0022(preol,appmod.coeffcores[pos])

    ## CONTRACT BEGINNING BELOW
    # Initialize: If at the lowest, need to initialize as diagonal
    if position!=mod.nleg-1:
        untenrechts=np.tensordot(mod.outcore,appmod.outcore,axes=[2,2])
        untenlinks=np.tensordot(appmod.outcore.copy(),appmod.outcore.copy(),axes=[2,2])
    else:
        untenrechts=np.zeros((mod.outdim,mod.outdim,appmod.outdim,appmod.outdim))
        for l1 in range(mod.outdim):
            for l2 in range(appmod.outdim):
                untenrechts[l1,l1,l2,l2]=1

        untenlinks=np.zeros((appmod.outdim,appmod.outdim,appmod.outdim,appmod.outdim))
        for l1 in range(appmod.outdim):
            for l2 in range(appmod.outdim):
                untenlinks[l1,l1,l2,l2]=1

    for pos in range(mod.nleg-2,position,-1):
        preur=np.tensordot(untenrechts,mod.coeffcores[pos],axes=[0,1])
        untenrechts=contract1142(preur,appmod.coeffcores[pos])

        preul=np.tensordot(untenlinks,appmod.coeffcores[pos],axes=[0,1])
        untenlinks=contract1142(preul,appmod.coeffcores[pos])

    if position!=mod.nleg-1:
        prerhs=np.tensordot(mod.coeffcores[position],obenrechts,axes=[0,0])
    else:
        prerhs=np.tensordot(mod.outcore,obenrechts,axes=[0,0])

    rhs=np.tensordot(prerhs,untenrechts,axes=[0,0])

    olsh=obenlinks.shape
    ulsh=untenlinks.shape

    operator=np.zeros((olsh[1],ulsh[1],ulsh[2],ulsh[3],olsh[0],ulsh[0]))
    for e0 in range(olsh[0]):
        for e1 in range(ulsh[0]):
            for e2 in range(olsh[1]):
                for e3 in range(ulsh[1]):
                    for e4 in range(ulsh[2]):
                        for e5 in range(ulsh[3]):
                            operator[e2,e3,e4,e5,e0,e1]=obenlinks[e0,e2]*untenlinks[e1,e3,e4,e5]
    osh=operator.shape
    op=operator.reshape(osh[0]*osh[1]*osh[2]*osh[3],osh[4]*osh[5])

    rhssh=rhs.shape
    rhside=rhs.reshape(rhssh[0],rhssh[1]*rhssh[2]*rhssh[3]*rhssh[4])


    sol=np.zeros((osh[4]*osh[5],mod.ndim))

    for i in range(mod.ndim):
        partsol=np.linalg.lstsq(op,rhside[i,:],rcond=None)
        sol[:,i]=partsol[0]

    solution=sol.reshape(osh[4],osh[5],mod.ndim)

    oldcores=appmod.coeffcores.copy()
    if position!=mod.nleg-1:
        appmod.coeffcores[position]=solution
    else:
        appmod.outcore=solution

    return appmod

def contract0022(ten1,ten2):
    sh1=ten1.shape
    sh2=ten2.shape
    res=np.zeros((sh1[1],sh2[1]))
    for r0 in range(sh1[0]):
        for r2 in range(sh1[2]):
            for e1 in range(sh1[1]):
                for e2 in range(sh2[1]):
                    res[e1,e2]+=ten1[r0,e1,r2]*ten2[r0,e2,r2]
    return res

def contract1142(ten1,ten2):
    sh1=ten1.shape
    sh2=ten2.shape
    res=np.zeros((sh1[3],sh1[0],sh2[0],sh1[2]))
    for r0 in range(sh1[1]):
        for r3 in range(sh1[4]):
            for e1 in range(sh1[0]):
                for e2 in range(sh1[2]):

                    for e3 in range(sh1[3]):
                        for e4 in range(sh2[0]):
                            res[e3,e1,e4,e2]+=ten1[e1,r0,e2,e3,r3]*ten2[e4,r0,r3]
    return res

def treshold_TT_linear(model,newmodel,nit=1):
    modcon=ha.contract_cores(model)

    for it in range(nit):
        for position in range(model.nleg-1,-1,-1):
            newmodel=position_optimization_tresholding_linear(model,newmodel,position)
            newmodcon=ha.contract_cores(newmodel)

    print("Error,GTNorm of the tresholding")
    print(np.linalg.norm(modcon-newmodcon),np.linalg.norm(modcon))

    return newmodel

def iterative_hard_treshold_TT_linear(smod,y,nit,ntrit,genknown=False,genmod=None):
    """ PARAMETERS:
        smod    startmodel
        y       predictions to fit
        nit     number of total iterations
        ntrit   number of iterations for tresholding operation
        factor  update step length
        """
    newmodel=smod
    if genknown:
        gencon=ha.contract_cores(genmod)
    for i in range(nit):
        newmodel=fixpoint_iteration_linear(newmodel,y,adjustmu=True)

        appmodel=moTT.symbolic_model_TT(smod.nleg,smod.ndat,smod.ndim,smod.ran,smod.types,smod.outdim)
        appmodel.init_coeffcores_random()

        appmodel=treshold_TT_linear(newmodel,appmodel,ntrit)

        appmodel.set_points(smod.points)
        appmodel.set_datacores(smod.datatype)
        appmodel.forward_generation(comgradients=False)

        print("ITERATION",i)
        print("Prediction error:")
        print(np.linalg.norm(appmodel.generation-y))

        if genknown:
            print("Coefficient error:")
            appcon=ha.contract_cores(appmodel)
            print(np.linalg.norm(appcon-gencon))

        newmodel=appmodel
    print("Norm of y:",np.linalg.norm(y))
    return newmodel


#########################
## NONLINEAR CONTRACTIONS
## DOES NOT WORK SO FAR
## IMPLEMENT ADJUSTMU AS IN THE LINEAR THEORY
#########################

"""
Iterative Hard Tresholding for nonlinear measurement operator A and X in the TT format: min(l2(AX-y)) subject to X low rank
    1) W^(j+1)=X^j + [D_X AX]^*[y-A(X^j)] -> fixpoint_iteration_linear()
    2) X^(j+1)=H_r(W^(j+1)) -> treshold_TT_linear(): optimize low rank TT to approximate W
"""

def fixpoint_iteration_general(mod,y,mu=1):
    mod.forward_generation(start=0,comgradients=True)
    residuum=y-mod.generation

    ## Construct addmod, to be added to the mod ([D_X AX]^*[y-AX])
    newtypes=[]
    for l in range(mod.ndat*mod.outdim*mod.ran+1):
        for r in range(mod.ran):
            newtypes.append(mod.types[r])
    addmod=moTT.symbolic_model_TT(mod.nleg,mod.ndat,mod.ndim,mod.ran*mod.ndat*mod.outdim,outdim=mod.outdim)

    ##First Coeffcore
    firstcore=np.zeros((addmod.ran,addmod.ndim))
    first_grad=mod.compute_cum_gradient_A(0)

    for j in range(mod.ndat):
        for l in range(mod.outdim):
            pos=(j*mod.outdim+l)*mod.ran
            firstcore[pos:(pos+mod.ran),:]=first_grad[j,0,:,:,l]
    addmod.coeffcores[0,0]=firstcore

    ##Middle Coeffcores
    for k in range(1,mod.nleg-1):
        core=np.zeros((addmod.ran,addmod.ran,addmod.ndim))
        cum_grad=mod.compute_cum_gradient_A(k)
        for j in range(mod.ndat):
            for l in range(mod.outdim):
                pos=(j*mod.outdim+l)*mod.ran
                core[pos:(pos+mod.ran),pos:(pos+mod.ran),:]=cum_grad[j,:,:,:,l]
        addmod.coeffcores[k]=core

    ##Last Coeffcore
    lastcore=np.zeros((addmod.ran,addmod.outdim,addmod.ndim))
    last_grad=mod.compute_cum_gradient_A(mod.nleg-1)
    for j in range(mod.ndat):
        for l in range(mod.outdim):
            pos=(j*mod.outdim+l)*mod.ran
            lastcore[pos:(pos+mod.ran),l,:]=last_grad[j,:,l,:,l]*residuum[j,l]
    addmod.outcore=lastcore

    ## Construct X+mu*[D_X AX]^*[y-AX] as newmod
    types=mod.types[:mod.ran]+["mul" for r in range(mod.ndat*mod.outdim)]
    newmod=moTT.symbolic_model_TT(mod.nleg,mod.ndat,mod.ndim,mod.ran+addmod.ran,types=newtypes,outdim=mod.outdim)
    newmod.coeffcores[:,:mod.ran,:mod.ran,:]=mod.coeffcores
    newmod.coeffcores[1:,mod.ran:,mod.ran:,:]=mu*addmod.coeffcores[1:]
    newmod.coeffcores[0,0,mod.ran:,:]=mu*addmod.coeffcores[0,0]
    newmod.outcore[:mod.ran]=mod.outcore
    newmod.outcore[mod.ran:]=mu*addmod.outcore

    newmod.set_datacores_from_input(mod.datacores)
    return newmod

def position_optimization_tresholding_general(mod,appmod,position,length=0.01):
    appmod.forward_generation(comgradients=True)
    cum_grad=appmod.compute_cum_gradient_A(position)
    grad_L=-2*hc.contract_fy(cum_grad,appmod.generation,mod.generation)

    #print(appmod.coeffcores[position])
    #print(grad_L)
    if position!=appmod.nleg-1:
        appmod.coeffcores[position]=appmod.coeffcores[position]-length*grad_L
    else:
        appmod.outcore=appmod.outcore-length*grad_L
    #print(appmod.coeffcores[position])
    return appmod


def treshold_TT_general(model,newmodel,ntrit=1,trlength=0.01):
    for trit in range(ntrit):
        for pos in range(model.nleg):
            newmodel=position_optimization_tresholding_general(model,newmodel,pos,trlength)
    return newmodel

def iterative_hard_treshold_TT_general(smod,y,nit,ntrit,factor,genknown=False,genmod=None,trlength=0.01):
    newmodel=smod
    if genknown:
        gencon=ha.contract_cores(genmod)
    for i in range(nit):
        newmodel=fixpoint_iteration_general(newmodel,y,mu=factor)
        newmodel.set_datacores_from_input(smod.datacores)
        newmodel.forward_generation(comgradients=False)

        appmodel=moTT.symbolic_model_TT(smod.nleg,smod.ndat,smod.ndim,smod.ran,smod.types,smod.outdim)
        appmodel.init_coeffcores_random()

        appmodel=treshold_TT_general(newmodel,appmodel,ntrit,trlength)

        appmodel.set_points(smod.points)
        appmodel.set_datacores(smod.datatype)
        appmodel.forward_generation(comgradients=False)

        print("ITERATION",i)
        print("Prediction error:")
        print(np.linalg.norm(appmodel.generation-y))

        if genknown:
            print("Coefficient error:")
            appcon=ha.contract_cores(appmodel)
            print(np.linalg.norm(appcon-gencon))

        newmodel=appmodel
    print("Norm of y:",np.linalg.norm(y))
    return newmodel


#########################
## MANDY UPDATE FOR LINEAR THEORY
#########################

def iterative_hard_tresholding_TT_mandy(smod,y,nit,ntrit,genknown=False,genmod=None,trlength=0.01):
    matrix=contract_datacores(smod)
    matinv=np.linalg.inv(matrix)

    print(np.matmul(matinv,matrix[0]))

    newmodel=smod
    if genknown:
        gencon=ha.contract_cores(genmod)

    for i in range(nit):
        print("ITERATION",i)

        newmodel.set_datacores_from_input(smod.datacores)
        newmodel.forward_generation(comgradients=False)
        res=y-newmodel.generation
        transres=np.tensordot(matinv,res,axes=[1,0])
        #print(transres)

        newmodel=fixpoint_iteration_linear(newmodel,y,mu=1,adjustmu=False,com_res=False,residuum=transres)
        newmodel.forward_generation(comgradients=False)

        print("Prediction error after update:")
        print(np.linalg.norm(newmodel.generation-y)/np.linalg.norm(y))

        appmodel=moTT.symbolic_model_TT(smod.nleg,smod.ndat,smod.ndim,smod.ran,smod.types,smod.outdim)
        appmodel.init_coeffcores_random()

        appmodel=treshold_TT_general(newmodel,appmodel,ntrit,trlength)

        appmodel.set_points(smod.points)
        appmodel.set_datacores(smod.datatype)
        appmodel.forward_generation(comgradients=False)


        print("Prediction error after tresholding:")
        print(np.linalg.norm(appmodel.generation-y)/np.linalg.norm(y))

        if genknown:
            print("Coefficient error:")
            appcon=ha.contract_cores(appmodel)
            print(np.linalg.norm(appcon-gencon)/np.linalg.norm(gencon))

        newmodel=appmodel

    return newmodel
