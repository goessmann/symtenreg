import model_TT as moTT
import helpers_analysis as ha


import numpy as np


def integrationstep(gen_mod,point,lam=0.001):
    gen_mod.set_points(point)
    gen_mod.set_datacores_legendre()
    gen_mod.forward_generation(0,comgradients=False)
    #print(gen_mod.generation)
    return point+lam*gen_mod.generation,gen_mod.generation


def generate_dynamics(gen_mod,startpoints,iterations,lam):
    points=np.zeros((iterations*len(startpoints),gen_mod.outdim))
    #points[0,:]=startpoint
    ys=np.zeros((iterations*len(startpoints),gen_mod.outdim))
    startcount=0
    for s in startpoints:
        points[startcount*iterations,:]=s
        point=np.zeros((1,gen_mod.outdim))
        point[0]=s
        for k in range(startcount*iterations+1,startcount*iterations+iterations+1):
            point,y=integrationstep(gen_mod,point,lam)
            if k<startcount*iterations+iterations:
                points[k,:]=point
                ys[k-1]=y
            else:
                ys[k-1]=y
        startcount+=1
    return points,ys


#########
#TEST PARAMETERS
#########

nleg=4          ## Order of the Coefficient Tensor
ndim=2          ## Number of basisfunctions
ndat=5       ## Number of snapshots per start
nstart=50       ## Number of starts
ran=3           ## Rank of the Coefficient Tensor (TT)
outdim=nleg     ## Outputdimension equal to variable number to interpret model as an dynamical system
magn=1        ## Magnitude of initialized coefficients
lam=1           ## Stepwidth in dynamic generation
types=['mul','mul','mul']  ## Types of the nonlinearities



gen_mod=moTT.symbolic_model_TT(nleg,1,ndim,ran,types,outdim)

## Sparse tensor corresponding to sigma((f1*f2),f3)+f4
c=np.zeros((nleg-1,ran,ran,ndim))
c[0,0,0]=magn*np.random.rand(ndim)
c[1,0,1]=magn*np.random.rand(ndim)
c[2,1,2]=magn*np.random.rand(ndim)

outcore=np.zeros((ran,outdim,ndim))
outcore[2,0]=magn*np.random.rand(ndim)
outcore[2,1]=magn*np.random.rand(ndim)
outcore[2,2]=magn*np.random.rand(ndim)
outcore[2,3]=magn*np.random.rand(ndim)
gen_mod.init_coeffcores(c,outcore)

startpoint=np.random.rand(nstart,nleg)



#print(generate_dynamics(gen_mod,startpoint,100,0.01))

reg_mod=moTT.symbolic_model_TT(nleg,ndat*nstart,ndim,ran,types,outdim)
reg_mod.init_coeffcores_random(magnitude=magn)


points,ys=generate_dynamics(gen_mod,startpoint,ndat,lam)
print(points)
print(ys)


reg_mod.set_points(points)
reg_mod.set_datacores_legendre()
reg_mod.forward_generation(comgradients=True)
#print(reg_mod.generation)

reg_mod.backward_iteration(ys,10,strategy='ALS',lam=1)
print(reg_mod.generation[:3],ys[:3])


con_reg=ha.contract_cores(reg_mod)
con_gen=ha.contract_cores(gen_mod)
print('Error coeffnorm, coeffnorm:',np.linalg.norm(con_reg-con_gen),np.linalg.norm(con_gen))
