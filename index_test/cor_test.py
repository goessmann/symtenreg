from __future__ import absolute_import, division, print_function, unicode_literals

import model_CP as moCP

import thresholding_TT as trTT
import helpers_core_optimization as hc
import helpers_nonlinear as hn

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import math

nit=0              ## Number of optimization sweeps
#nleg=10             ## Order of the Coefficient Tensor
ndim=2              ## Number of basisfunctions
ndat=10            ## Number of snapshots
ran=1               ## Rank of the Coefficient Tensor (TT)
outdim=1            ## Output dimension of the model

checklength=2

types=[['mul' for l in range(ran)] for i in range(checklength)]    ## Types of the combinations
types=np.array(types)
#types[nleg-1,0]="mul"
types[1,0]="ReLU"


legendrecorhistory=np.zeros(checklength)
gaussiancorhistory=np.zeros(checklength)
orthogonalcorhistory=np.zeros(checklength)

orthogonalcoeffs=np.zeros((checklength,outdim,ndim,ran))
coeffcores=np.zeros((checklength,outdim,ndim,ran))
for pos in range(checklength):
    for l in range(outdim):
        for k in range(ran):
            tworand=np.random.rand(ndim,2)
            q,r=np.linalg.qr(tworand)

            coeffcores[pos,l,:,k]=q[0]
            orthogonalcoeffs[pos,l,:,k]=q[1]
            #coeffcores[pos,l,:,k]=core/np.linalg.norm(core)



for nleg in range(1,checklength+1):

    points=np.random.rand(ndat,nleg)

    legendremod=moCP.symbolic_model_CP(nleg,ndat,ndim,ran,outdim=outdim,types=types)
    legendremod.set_points(points)
    legendremod.set_datacores("legendre")
    legendremod.set_coeffcores(coeffcores[:nleg])

    gaussianmod=moCP.symbolic_model_CP(nleg,ndat,ndim,ran,outdim=outdim,types=types)
    gaussianmod.set_points(points)
    gaussianmod.set_datacores_gaussian()
    gaussianmod.set_coeffcores(coeffcores[:nleg])

    legendrecor=legendremod.lin_types_correlation()
    legendrecorhistory[nleg-1]=legendrecor

    gaussiancor=gaussianmod.lin_types_correlation()
    gaussiancorhistory[nleg-1]=gaussiancor

    orthogonalcor=gaussianmod.alt_lin_correlation(orthogonalcoeffs[:nleg])

    orthogonalcorhistory[nleg-1]=orthogonalcor

    orthpreds=gaussianmod.com_prediction_alternative(orthogonalcoeffs[:nleg])

    print(nleg,legendrecor,gaussiancor,orthogonalcor)




figsize = (5,3)
fig1 = plt.figure(1, figsize=figsize)
plt.plot([l for l in range(checklength)],legendrecorhistory,"g+")
plt.plot([l for l in range(checklength)],gaussiancorhistory,"r^")
plt.plot([l for l in range(checklength)],orthogonalcorhistory,"b+")
#plt.show()




