import helpers_nonlinear as hn

def MC_correlation(mod):
    cortestmodel=moCP.symbolic_model_CP(mod.nleg,ndat=mod.ndat,ndim=mod.ndim,ran=mod.ran,outdim=mod.outdim)
    cortestmodel.set_datacores_from_input(mod.datacores)
    cortestmodel.set_coeffcores(mod.coeffcores)

    corpred=cortestmodel.com_prediction_linear()
    pred=cortestmodel.com_prediction_types()

    cor=0
    n1=0
    n2=0
    for j in range(ndat):
        cor+=corpred[j,0]*pred[j,0]
        n1+=corpred[j,0]**2
        n2+=pred[j,0]**2
    cor=cor/(math.sqrt(n1)*math.sqrt(n2))

    return cor
