from __future__ import absolute_import, division, print_function, unicode_literals

import model_TT as moTT

import thresholding_TT as trTT
import helpers_core_optimization as hc
import helpers_nonlinear as hn
import helpers_analysis as ha

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import math

nit=10              ## Number of optimization sweeps
size=1

nleg=10             ## Order of the Coefficient Tensor
ndim=2              ## Number of basisfunctions
ndat=10000            ## Number of snapshots
ran=3               ## Rank of the Coefficient Tensor (TT)
outdim=1            ## Output dimension of the model


types=['mul' for l in range(ran)]     ## Types of the combinations
types=np.array(types)
types[0]="plus"

#types[nleg-1,0]="plus"
#types[1,0]="plus"

coeffcores=np.zeros((nleg,outdim,ndim,ran))
for pos in range(nleg):
    for l in range(outdim):
        for k in range(ran):
            core=np.random.rand(ndim)
            coeffcores[pos,l,:,k]=core/np.linalg.norm(core)

points=np.random.rand(ndat,nleg)

## Generationmodel
genmod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,outdim=outdim,types=types)
genmod.set_points(points)
genmod.set_datacores("legendre")
genmod.init_coeffcores_random()
pred=genmod.forward_generation()


## Aproximationmodel: Same Datacores, Different Coeffcores
approxmodel=moTT.symbolic_model_TT(nleg,ndat=ndat,ndim=ndim,ran=ran,outdim=outdim,types=["mul" for l in range(ran)])
approxmodel.set_points(points)
approxmodel.set_datacores("legendre")
approxmodel.init_coeffcores_random()
approxmodel.forward_generation()


figsize = (5,3)
fig1 = plt.figure(1, figsize=figsize)
plt.plot(genmod.generation,approxmodel.generation,"g+")
plt.title("TT nonlinear vs trained linear observations")
plt.ylabel('Nonlinear Generation')
plt.xlabel('Multilinear Interpolation')
#plt.yscale('log')
plt.show()




print("Before")
appcon=ha.contract_cores(approxmodel)
appconnormed=appcon/np.linalg.norm(appcon)

gencon=ha.contract_cores(genmod)
genconnormed=gencon/np.linalg.norm(gencon)

print(np.linalg.norm(appconnormed-genconnormed))


## Optimize the TT
approxmodel.backward_iteration(pred,nit=nit,lam=1)



print("Afterwards")

appcon=ha.contract_cores(approxmodel)
appconnormed=appcon/np.linalg.norm(appcon)

print(np.linalg.norm(appconnormed-genconnormed))



#figsize = (5,3)
#fig1 = plt.figure(1, figsize=figsize)
#plt.plot(pred,corpred,"g+")
#plt.show()

## Plot prediction
figsize = (5,3)
fig1 = plt.figure(1, figsize=figsize)
plt.plot(genmod.generation,approxmodel.generation,"g+")
#plt.plot([l for l in range(nit)],prederrorhistory,"r+")
#savename=str(nleg)+types[0,0]#+types[1,0]+types[2,0]+types[nleg-3,0]+types[nleg-2,0]+types[nleg-1,0]
plt.title("TT nonlinear vs trained linear observations")

plt.ylabel('Nonlinear Generation')
plt.xlabel('Multilinear Interpolation')


#plt.yscale('log')
plt.show()
fig1.savefig("index_test/plots/"+types[0]+str(ran)+".png", dpi=300, bbox_inches='tight')
