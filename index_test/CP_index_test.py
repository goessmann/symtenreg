from __future__ import absolute_import, division, print_function, unicode_literals

import model_CP as moCP

import thresholding_TT as trTT
import helpers_core_optimization as hc
import helpers_nonlinear as hn

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import math

nit=10              ## Number of optimization sweeps
size=100

nleg=10             ## Order of the Coefficient Tensor
ndim=100              ## Number of basisfunctions
ndat=1000            ## Number of snapshots
ran=1               ## Rank of the Coefficient Tensor (TT)
outdim=1            ## Output dimension of the model


types=[['plus' for l in range(ran)] for i in range(nleg)]    ## Types of the combinations
types=np.array(types)

#types[nleg-1,0]="plus"
#types[1,0]="plus"

coeffcores=np.zeros((nleg,outdim,ndim,ran))
for pos in range(nleg):
    for l in range(outdim):
        for k in range(ran):
            core=np.random.rand(ndim)
            coeffcores[pos,l,:,k]=core/np.linalg.norm(core)

points=np.random.rand(ndat,nleg)

## Generationmodel
genmod=moCP.symbolic_model_CP(nleg,ndat,ndim,ran,outdim=outdim,types=types)
genmod.set_points(points)

## GAUSSIAN DATACORES
#genmod.set_datacores("legendre")
genmod.set_datacores_gaussian()
genmod.set_coeffcores(coeffcores)
pred=genmod.com_prediction_types()

## Aproximationmodel: Same Datacores, Different Coeffcores
approxmodel=moCP.symbolic_model_CP(nleg,ndat=ndat,ndim=ndim,ran=ran,outdim=outdim)
approxmodel.set_datacores_from_input(genmod.datacores)
approxmodel.set_coeffcores(np.random.rand(nleg,outdim,ndim,ran))
approxmodel.com_prediction_linear()



figsize = (5,3)
fig1 = plt.figure(1, figsize=figsize)
plt.plot(pred,approxmodel.predictions_linear,"g+")
#plt.plot([l for l in range(nit)],prederrorhistory,"r+")
#savename=str(nleg)+types[0,0]#+types[1,0]+types[2,0]+types[nleg-3,0]+types[nleg-2,0]+types[nleg-1,0]
plt.title("CP nonlinear vs trained linear observations")

plt.ylabel('Nonlinear Generation')
plt.xlabel('Multilinear Interpolation')


#plt.yscale('log')
plt.show()




print("##### ERROR")
print(np.linalg.norm(approxmodel.coeffcores-genmod.coeffcores))

prederrorhistory=np.zeros(nit)
coeferrorhistory=np.zeros(nit)

for it in range(nit):
    print("iteration",it)
    prederror=approxmodel.backward_iteration(pred,size=size,com_error=True)
    prederrorhistory[it]=prederror

    norm_coeffcore=np.zeros((nleg,outdim,ndim,ran))
    for pos in range(nleg):
        for l in range(outdim):
            for k in range(ran):
                norm_coeffcore[pos,l,:,k]=approxmodel.coeffcores[pos,l,:,k]/np.linalg.norm(approxmodel.coeffcores[pos,l,:,k])

    print("##### COEFFICIENT ERROR")
    coefferror=np.linalg.norm(norm_coeffcore-genmod.coeffcores)

    print(coefferror)
    coeferrorhistory[it]=coefferror


approxmodel.com_prediction_linear()


figsize = (5,3)
fig1 = plt.figure(1, figsize=figsize)
plt.plot(pred,approxmodel.predictions_linear,"g+")
#plt.plot([l for l in range(nit)],prederrorhistory,"r+")
#savename=str(nleg)+types[0,0]#+types[1,0]+types[2,0]+types[nleg-3,0]+types[nleg-2,0]+types[nleg-1,0]
plt.title("CP nonlinear vs trained linear observations")

plt.ylabel('Nonlinear Generation')
plt.xlabel('Multilinear Interpolation')


#plt.yscale('log')
plt.show()




## Plot prediction
figsize = (5,3)
fig1 = plt.figure(1, figsize=figsize)
plt.plot([l for l in range(nit)],coeferrorhistory,"g+")
plt.plot([l for l in range(nit)],prederrorhistory,"r+")
savename=str(nleg)+types[0,0]#+types[1,0]+types[2,0]+types[nleg-3,0]+types[nleg-2,0]+types[nleg-1,0]
plt.title(savename)
#fig1.savefig("index_test/plots/"+savename+".png", dpi=300, bbox_inches='tight')
plt.yscale('log')
plt.show()
