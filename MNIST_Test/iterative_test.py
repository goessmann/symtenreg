from __future__ import absolute_import, division, print_function, unicode_literals


#import model_filter as mofil
import model_CP as moCP

import thresholding_TT as trTT
import helpers_core_optimization as hc
import helpers_nonlinear as hn

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt


print(tf.__version__)

mnist = keras.datasets.mnist

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

train_images = train_images / 255.0
test_images = test_images / 255.0

#print(test_labels)


trains=train_images.shape
tests=test_images.shape

train_reshaped=train_images.reshape(trains[0],trains[1]*trains[2])
test_reshaped=test_images.reshape(tests[0],trains[1]*trains[2])

y=np.zeros((train_images.shape[0],10))
for j in range(trains[0]):
    y[j,train_labels[j]]=1

## PARAMTER OF TT CORE
nleg=trains[1]*trains[2]      ## Order of the Coefficient Tensor
ndim=2                        ## Number of basisfunctions
ndat=trains[0]                     ## Number of snapshots
ran=trains[0]                         ## Rank of the Coefficient Tensor (TT)
outdim=10
types=['mul' for j in range(ran)]

nit=1
MANDysize=10     # Number of random training for MANDyoptimization
filtersize=10    # Number of random training for filteroptimization
wsize=10         # Size of the Filter Window
length=1      # Steplength of the Gradient optimizer of the Filter
filternit=3
#plength=28      # Pixellength of the Data (needed for Windowcomputation)

filter=np.identity(nleg)

#imagemodel=mofil.filter_model(nleg,ndat,ndim,outdim,filter=filter)
#errorhistory=imagemodel.iterative_MANDY_filteropt(train_reshaped,y,nit,
#            length=length,MANDysize=MANDysize,filtersize=filtersize,wsize=wsize,filternit=filternit)

imagemodel=moCP.symbolic_model_CP(nleg,ndat,ndim,outdim)

errorhistory=imagemodel.iterative_MANDY_filteropt(train_reshaped,y,nit,
            length=length,MANDysize=MANDysize,filtersize=filtersize,wsize=wsize,filternit=filternit)



figsize = (5,3)
fig1 = plt.figure(1, figsize=figsize)
plt.plot(errorhistory,"g+")
savename="Window "+str(wsize)+" Sizes "+str(filtersize)+" "+str(MANDysize)+" Length "+str(length)+" Nit "+str(nit)
plt.title(savename)
fig1.savefig("errorplots/"+savename+".png", dpi=300, bbox_inches='tight')
plt.show()
