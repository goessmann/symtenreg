import helpers_nonlinear as hn
import helpers_core_optimization as hc
import helpers_analysis as ha
import thresholding_TT as trTT


import numpy as np

class symbolic_model_TT():

    """ Parameters:
            nleg    Order of the coefficient tensor
            ran     TT-Rank of the coefficient tensor
            ndat    Number of snapshots
            ndim    Number of basisfunctions (Legendre polynomials by default)
            types   Operations in the generalized core contraction
            outdim  Output dimension of the model (=nleg for governing equations)

        Attributes:
            coeffcores      Coefficient tensor cores (as numpy arrays), except last
            outcore         Last coefficient tensor core
            datacores       Datacores
            partfunctions   Generated function after partial contractions
            gradients_f     Gradients of the generated function wrt the previous function
            gradient_f_out
            gradients_A     Gradients of the generated function wrt the datacore
            gradient_A_out

            points          Snapshots
            generations     Resulting function after last contraction
            cum_gradient    Gradient of the generations wrt datacore
            loss            Frobenius distance of generated function and data
    """

    def __init__(self,nleg,ndat,ndim,ran,types=['mul','plus','ReLU'],outdim=1):
        self.nleg=nleg
        self.ran=ran
        self.ndat=ndat
        self.ndim=ndim
        self.types=types
        self.outdim=outdim

        self.coeffcores=np.zeros((self.nleg-1,self.ran,self.ran,self.ndim))
        self.outcore=np.zeros((self.ran,self.outdim,self.ndim))

        self.datacores=np.zeros((self.nleg,self.ndim,self.ndat))
        self.datatype="legendre"
        self.partfunctions=np.zeros((self.nleg-1,self.ndat,self.ran))

        self.gradients_f=np.zeros((self.nleg-1,self.ndat,self.ran,self.ran))
        self.gradient_f_out=np.zeros((self.ndat,self.ran,self.outdim))
        self.gradients_A=np.zeros((self.nleg-1,self.ndat,self.ran,self.ran,self.ndim))
        self.gradient_A_out=np.zeros((self.ndat,self.ran,self.outdim,self.ndim))

    def set_points_random(self,magnitude=2):
        self.points=np.random.rand(self.ndat,self.nleg)*magnitude-np.ones((self.ndat,self.nleg))*magnitude/2

    def set_points(self,points):
        self.points=points

    def set_datacores_from_input(self,datacores):
        self.datacores=datacores

    def set_datacores(self,type,withoutone=True):
        self.datatype=type
        for k in range(self.nleg):
            self.datacores[k,:,:]=hn.datacore(self.points,self.ndim,k,type,withoutone)


    def init_coeffcores_random(self,coeffcores=None,magnitude=0.1):
        for k in range(self.nleg-1):
                self.coeffcores[k,:,:,:]=np.random.rand(self.ran,self.ran,self.ndim)*magnitude-0.5*magnitude*np.ones((self.ran,self.ran,self.ndim))
        if self.nleg!=1:
            self.coeffcores[0,1:,:,:]=np.zeros((self.ran-1,self.ran,self.ndim))
        self.outcore=np.random.rand(self.ran,self.outdim,self.ndim)

    def init_coeffcores(self,coeffcores,outcore):
        self.coeffcores=coeffcores
        if self.nleg!=1:
            self.coeffcores[0,1:,:,:]=np.zeros((self.ran-1,self.ran,self.ndim))
        self.outcore=outcore

    def forward_generation(self,start=0,comgradients=False):
        """"
        Performs the generation steps, computes the gradients wrt partfunctions f and datacores A if comgradients.
        """
        if start==0:
            ## INTIALIZATION: FIRST PARTFUNCTION BY CONTRACTION OF A0 AND phi0
            # TAKE ONLY FIRST SLICE OF (l1)-AXES
            A0=self.coeffcores[0,0,:,:]
            phi0=self.datacores[0,:,:]
            fnew=np.tensordot(A0,phi0,axes=[1,0])
            fnew=fnew.transpose()
            self.partfunctions[0,:,:]=fnew

            if comgradients:
                self.compute_gradient_f(0)
                self.compute_gradient_A(0)

        for k in range(max(start,1),self.nleg-1):
            ## ITERATION: CONSTRUCT NEXT PARTFUNCTION BY NONLINEAR CONTRACTION OF FORMER WITH Ak AND phik
            fold=self.partfunctions[k-1,:,:]
            Ak=self.coeffcores[k,:,:,:]
            phik=self.datacores[k,:,:]
            fnew=hn.generation_step(fold,Ak,phik,self.types)
            self.partfunctions[k,:,:]=fnew

            if comgradients:
                self.compute_gradient_f(k)
                self.compute_gradient_A(k)

            #print("Forward generation number",k)

        ## OUTPUT GENERATION AS LAST FUNCTION
        # SET ONLY FIRST SLICE OF (l2)-AXES
        Ad=self.outcore
        phid=self.datacores[self.nleg-1,:,:]
        Adphid=np.tensordot(Ad,phid,axes=[2,0])
        fold=self.partfunctions[self.nleg-2,:,:]
        ffinal=np.zeros((self.ndat,self.outdim))
        for l in range(self.outdim):
            for j in range(self.ndat):
                a=fold[j,:]
                b=Adphid[:,l,j]
                count,output=hn.general_contraction(a,b,self.types)
                ffinal[j,l]=count
        #self.partfunctions[self.nleg-1,:,0]=ffinal
        self.generation=ffinal

        if comgradients:
            self.compute_gradient_f(self.nleg-1)
            self.compute_gradient_A(self.nleg-1)



        return self.generation

    def prediction_loss(self,y):
        self.loss=np.linalg.norm(self.generation-y)
        return self.loss

    def compute_gradient_f(self,position):
        if position==self.nleg-1:
            gradient=np.zeros((self.ndat,self.ran,self.outdim))
            fposmin=np.copy(self.partfunctions[position-1,:,:])
            Apos=np.copy(self.outcore)
            phipos=np.copy(self.datacores[position,:,:])
            Aposphipos=np.tensordot(Apos,phipos,axes=([2,0]))
            for j in range(self.ndat):
                for l1 in range(self.ran):
                    for l2 in range(self.outdim):
                        gradient[j,l1,l2]=hn.first_diff(fposmin[j,l1],Aposphipos[l1,l2,j],self.types[l1])
            self.gradient_f_out=np.copy(gradient)
            return gradient

        elif position==0:
            # NOT NEEDED
            return np.zeros((self.ndat,self.ran,self.ran))

        gradient=np.zeros((self.ndat,self.ran,self.ran))
        #fpos=np.copy(self.partfunctions[position,:,:])
        fposmin=np.copy(self.partfunctions[position-1,:,:])
        Apos=np.copy(self.coeffcores[position,:,:,:])
        phipos=np.copy(self.datacores[position,:,:])
        Aposphipos=np.tensordot(Apos,phipos,axes=([2,0]))

        ## SET THE GRADIENT FOR EACH DATAPOINT(j) AND IN-(l1) AND OUT-(l2)CHANNEL
        for j in range(self.ndat):
            for l1 in range(self.ran):
                for l2 in range(self.ran):
                    gradient[j,l1,l2]=hn.first_diff(fposmin[j,l1],Aposphipos[l1,l2,j],self.types[l1])
        self.gradients_f[position,:,:,:]=np.copy(gradient)
        return gradient

    def compute_gradient_A(self,position):
        ## LAST LEG SPECIAL CASE: (l2)-AXES HAS OUTPUT DIMENSION
        if position==self.nleg-1:
            gradient=np.zeros((self.ndat,self.ran,self.outdim,self.ndim))
            fposmin=self.partfunctions[position-1,:,:]
            Apos=self.outcore
            phipos=self.datacores[position,:,:]
            Aposphipos=np.tensordot(Apos,phipos,axes=([2,0]))
            for j in range(self.ndat):
                for l1 in range(self.ran):
                    for l2 in range(self.outdim):
                        for ipos in range(self.ndim):
                            gradient[j,l1,l2,ipos]=hn.second_diff(fposmin[j,l1],Aposphipos[l1,l2,j],self.types[l1])*phipos[ipos,j]
            self.gradient_A_out=gradient
            return gradient

        ## FIRST LEG SPECIAL CASE: (l1)-AXES ONE DIM, INDEPENDENT ON (l2)-AXES
        elif position==0:
            gradient=np.zeros((self.ndat,self.ran,self.ran,self.ndim))
            phi0=self.datacores[0,:,:]
            for j in range(self.ndat):
                for l2 in range(self.ran):
                    for ipos in range(self.ndim):
                            gradient[j,0,l2,ipos]=phi0[ipos,j]
            self.gradients_A[position,:,:,:,:]=gradient
            return gradient

        fposmin=self.partfunctions[position-1,:,:]
        Apos=self.coeffcores[position,:,:,:]
        phipos=self.datacores[position,:,:]
        Aposphipos=np.tensordot(Apos,phipos,axes=([2,0]))

        ## SET THE GRADIENT FOR EACH DATAPOINT(j) AND IN-(l1) AND OUT-(l2)CHANNEL
        ## ADDITIONAL IN THE A CASE: AFTERDIFFERENTIATION WITH PHIPOS (ipos)
        gradient=np.zeros((self.ndat,self.ran,self.ran,self.ndim))
        for j in range(self.ndat):
            for l1 in range(self.ran):
                for l2 in range(self.ran):
                    for ipos in range(self.ndim):
                        gradient[j,l1,l2,ipos]=hn.second_diff(fposmin[j,l1],Aposphipos[l1,l2,j],self.types[l1])*phipos[ipos,j]
        self.gradients_A[position,:,:,:,:]=gradient

        if position==self.nleg-1:
            self.gradients_A[position,:,:,1:,:]=np.zeros((self.ndat,self.ran,self.ran-1,self.ndim))
        return gradient

    def compute_cum_gradient_A(self,position):
        """
        :param position: Position of the datacore, to which the cumulative gradient is computed
        :return:    cumulative gradient
        """

        ## SPECIAL CASE OF LAST POSITION
        if position==self.nleg-1:
            cum_gradient=np.zeros((self.ndat,self.ran,self.outdim,self.ndim,self.outdim))
            for j in range(self.ndat):
                for l1 in range(self.ran):
                    for l2 in range(self.outdim):
                        for i0 in range(self.ndim):
                            cum_gradient[j,l1,l2,i0,l2]=self.gradient_A_out[j,l1,l2,i0]
            self.cum_gradient=cum_gradient
            return cum_gradient

        ## START IN THE BACK AND PERFORM GRADIENT MULTIPLICATION
        old_gradient=self.gradient_f_out
        for k in range(self.nleg-2,position,-1):
            ## IF FURTHER THAN LAST-2 POSITION NEED TO CONCANATE THE GRADIENTS_f
            new_gradient=np.zeros((self.ndat,self.ran,self.outdim))
            for j in range(self.ndat):
                for l1 in range(self.ran):
                    for l2 in range(self.outdim):
                        new_gradient[j,l1,l2]=np.tensordot(self.gradients_f[k,j,l1,:],old_gradient[j,:,l2],axes=([0,0]))
            old_gradient=new_gradient

        cum_gradient=np.zeros((self.ndat,self.ran,self.ran,self.ndim,self.outdim))
        for j in range(self.ndat):
            for l1 in range(self.ran):
                for l2 in range(self.ran):
                    for l3 in range(self.outdim):
                        for ik in range(self.ndim):
                            cum_gradient[j,l1,l2,ik,l3]=self.gradients_A[position,j,l1,l2,ik]*old_gradient[j,l2,l3]
        self.cum_gradient=cum_gradient
        return cum_gradient

    def backward_iteration(self,y,nit=1,lam=1,strategy='ALS',reject=True):
        """"
        Iterate the Optimization procedure

        Parameters:
            nit         Number of iterations
            lam         Steplength to the minimization
            strategy    Optimization by Alternating Least Squares or Gradient Descent

        """
        if strategy=='ALS':
            for k in range(nit):
                for position in range(self.nleg-1,-1,-1):
                    hc.position_optimization_ALS(self,position,y,lam,forward=True,comgradients=True,rejection=reject)
                ## REMOVE REDUNDANCY IN FIRST CORE
                self.coeffcores[0,1:,:,:]=np.zeros((self.ran-1,self.ran,self.ndim))

                print(k,np.linalg.norm(y-self.generation))

        if strategy=='GD':
            for k in range(nit):
                data=[i for i in range(self.ndat)]
                for position in range(self.nleg-1,-1,-1):
                    hc.position_optimization_GD(self,position,y,lam,data,rejection=reject)
                ## REMOVE REDUNDANCY IN FIRST CORE
                self.coeffcores[0,1:,:,:]=np.zeros((self.ran-1,self.ran,self.ndim))

                print(k,np.linalg.norm(y-self.generation))


    def randomized_backward_iteration(self,y,size,nit,lam,strategy,
                                      datatype="legendre",
                                      reject=True,
                                      measure_coef_er=False,
                                      gen_mod=None):

        oldndat=self.ndat
        pred_er_history=np.zeros(nit)
        coef_er_history=np.zeros(nit)

        if measure_coef_er:
            gen_con=ha.contract_cores(gen_mod)

        for k in range(nit):
            ## Each Iteration different random choice of datapoints
            randomchoice=np.random.choice(oldndat,size,replace=False)
            randompoints=np.zeros((size,self.nleg))

            ## Generate ysized
            ysized=np.zeros((size,self.outdim))
            pos=0
            for l in randomchoice:
                randompoints[pos]=self.points[l]
                ysized[pos,:]=y[l,:]
                pos+=1

            self.datacores=np.zeros((self.nleg,self.ndim,size))
            self.ndat=size

            for i in range(self.nleg):
                self.datacores[i,:,:]=hn.datacore(randompoints,self.ndim,i,self.datatype,withoutone=True)

            ## Initialize everything to new size
            self.partfunctions=np.zeros((self.nleg-1,size,self.ran))
            self.forward_generation(comgradients=False)
            self.gradients_f=np.zeros((self.nleg-1,size,self.ran,self.ran))
            self.gradient_f_out=np.zeros((size,self.ran,self.outdim))
            self.gradients_A=np.zeros((self.nleg-1,size,self.ran,self.ran,self.ndim))
            self.gradient_A_out=np.zeros((size,self.ran,self.outdim,self.ndim))
            self.generation=np.zeros((size,self.outdim))
            self.forward_generation(comgradients=True)

            if strategy=='ALS':
                for position in range(self.nleg-1,-1,-1):
                    hc.position_optimization_ALS(self,position,ysized,lam,forward=True,comgradients=True,rejection=reject)

            if strategy=='GD':
                ## HAVE ALREADY REDUCED THE DATA, THUS HAVE TO TAKE HERE ALL
                data=[i for i in range(self.ndat)]
                for position in range(self.nleg-1,-1,-1):
                    hc.position_optimization_GD(self,position,ysized,lam,data,rejection=reject)

            if strategy=="IHT_lin":
                newmodel=trTT.fixpoint_iteration_linear(self,ysized,mu=1,adjustmu=False)
                treshmodel=trTT.treshold_TT_linear(newmodel,self,nit=100)

            ## REMOVE REDUNDANCY IN FIRST CORE
            self.coeffcores[0,1:,:,:]=np.zeros((self.ran-1,self.ran,self.ndim))

            pred_error=np.linalg.norm(ysized-self.generation)/np.linalg.norm(self.generation)
            pred_er_history[k]=pred_error
            print(k,pred_error)

            if measure_coef_er:
                #gen_con=ha.contract_cores(gen_mod)
                mod_con=ha.contract_cores(self)
                coef_error=np.linalg.norm(gen_con-mod_con)/np.linalg.norm(gen_con)
                coef_er_history[k]=coef_error

        return pred_er_history,coef_er_history
