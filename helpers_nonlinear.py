import numpy as np

## NOT USED SO FAR
def nonlinearity(x,type):
    if type=="sin":
        return np.sin(x)
    if type=="cos":
        return np.cos(x)
    if type=="exp":
        return np.exp(x)
    if type=="square":
        return x**2
    if type=="linear":
        return x
    if type=="inverse":
        if x!=0:
            return 1/x
        else:
            return 0

# LEGENDRE POLYNOMIALS
def legendre(x,i):
    if i==0:
        #print('USED CONSTANT')
        return 1
    if i==1:
        return x
    if i==2:
        return 1.5*(x**2)-0.5
    if i==3:
        return 2.5*(x**3)-1.5*x
    if i==4:
        return (35*(x**4)-30*(x**2)+3)

def monomial(x,i):
    return x**i

def fourier(x,i):
    return np.cos(np.pi*x*i)

def sincos(x,i):
    if i==0:
        return np.sin(x)
    else:
        return np.cos(x)

def datacore(points,ndim,k,type,withoutone=False):
    ndat=len(points)
    a0=np.zeros((ndim,ndat))
    if withoutone:
        for i in range(ndim):
            for j in range(ndat):
                if type=="legendre":
                    a0[i,j]=legendre(points[j,k],i+1)
                if type=="monomial":
                    a0[i,j]=monomial(points[j,k],i+1)
                if type=="fourier":
                    a0[i,j]=fourier(points[j,k],i+1)
                if type=="sincos":
                    a0[i,j]=sincos(points[j,k],i)
    else:
        for i in range(ndim):
            for j in range(ndat):
                if type=="legendre":
                    a0[i,j]=legendre(points[j,k],i)
                if type=="monomial":
                    a0[i,j]=monomial(points[j,k],i)
                if type=="fourier":
                    a0[i,j]=fourier(points[j,k],i)
                if type=="sincos":
                    a0[i,j]=sincos(points[j,k],i)

    return a0

# CONTRACTION OPERATIONS
def general_operation(x,y,type):
    if type=='mul':
        return x*y
    if type=='plus' or type=="plu":
        return x+y
    if type=='div':
        return x/y
    if type=='ReLU' or type=="ReL":
        return np.maximum(x+y,0)
    if type=='ReMU' or type=="ReM":
        return np.maximum(x*y,0)
    if type=='sin':
        return np.sin(x+y)
    if type=='cos':
        return np.cos(x+y)
    if type=='pot':
        return y**x



# GENERALIZED VECTOR CONTRACTION
def general_contraction(a,b,types):
    count=0
    output=np.zeros(len(a))
    for j in range(len(a)):
        output[j]=general_operation(a[j],b[j],types[j])
        count+=output[j]
    return count, output

# ONE STEP IN THE GENERATION PROCESS
def generation_step(fkold,Ak,phik,types=['mul','plus','ReLU']):
    ranold=Ak.shape[0]
    rannew=Ak.shape[1]
    ndat=phik.shape[1]

    ## LINEAR CONTRACTION OF AK AND PHIK ALONG NDIM AXES, RESULT:
    #FIRST RANOLD, SECOND RANNEW, THIRD DAT
    Akphik=np.tensordot(Ak,phik,axes=([2,0]))

    ## NONLINEAR CONTRACTION WITH FKOLD TO GET FKNEW
    fknew=np.zeros((ndat,rannew))
    for l in range(rannew):
        for j in range(ndat):
            a=fkold[j,:]
            b=Akphik[:,l,j]
            count,output=general_contraction(a,b,types)
            fknew[j,l]=count
    return fknew

# DIFFERENTIATION WRT THE FIRST COORDINATE
def first_diff(x,y,type):
    if type=='mul':
        return y
    if type=='plus' or type=="plu":
        return 1
    if type=='div':
        return 1/y
    if type=='ReLU':
        if x+y<=0:
            return 0
        else:
            return 1
    if type=='ReMU':
        if x*y<=0:
            return 0
        else:
            return y
    if type=='sin':
        return np.cos(x+y)
    if type=='cos':
        return -np.sin(x+y)
    if type=='pot':
        return np.log(y)*(y**x)

# DIFFERENTIATION WRT THE SECOND COORDINATE
def second_diff(x,y,type):
    if type=='mul':
        return x
    if type=='plus' or type=="plu":
        return 1
    if type=='div':
        return -x/(y**2)
    if type=='ReLU' or type=="ReL":
        if x+y<=0:
            return 0
        else:
            return 1
    if type=='ReMU' or type=="ReM":
        if x*y<=0:
            return 0
        else:
            return x
    if type=='sin':
        return np.cos(x+y)
    if type=='cos':
        return -np.sin(x+y)
    if type=='pot':
        if y!=0:
            return (1/y)*(y**(x-1))
        else:
            return 0

#def ht_lposition(nlay,npos,laynumber,position):

def diff(x,type):
    if type=="cos":
        return np.sin(x)
    if type=="sin":
        return -np.cos(x)















##### HT BUILDINGS

def ht_coef_number(nleg):
    lg2=np.log2(nleg)
    return int(2**(lg2)-2)

def ht_lay_number(nleg):
    return int(np.log2(nleg)-1)

def ht_layer(position,nleg):
    cnumber=ht_coef_number(nleg)
    return int(np.floor(np.log2(cnumber+1-position)))

def ht_pos_in_layer(position,nleg):
    cnumber=ht_coef_number(nleg)
    lay=ht_layer(position,nleg)
    leftpos=cnumber-position
    n_upper_lay=2**(np.floor(np.log2(leftpos+2))-1)
    #print(n_upper_lay)



    npos_in_upper=2
    #print(npos_in_upper)
    #print(2**(lay-1)-(cnumber-position-difference))
    #return 2**lay-(cnumber-position-difference)


def ht_position(laynumber,pos_in_lay):
    return pos

#print(ht_pos_in_layer(4,8))




def ht_neighbors(nleg):
    lg2=np.log2(nleg)
    cnumber=ht_coef_number(nleg)
    lnumber=ht_lay_number(nleg)
    print(lnumber)
    neighbores=np.zeros((cnumber,3))
    laynum=lg2
    for l in range(cnumber):
        laynum=int(np.floor(np.log2(cnumber+1-l)))
        #lpos=0

        #pos=int(2**(lnumber-np.log2(cnumber)))
        #print(laynum)
    #First layer: Init to the datacores


    return neighbores
    #for lay in range(laynum):


