import model_TT as moTT

import matplotlib.pyplot as plt
import math
import numpy as np

def generation_model_TT(nleg,ndat,ndim,ran,types,coefficients,points):
    gen_mod=moTT.symbolic_model_TT(nleg,ndat,ndim,ran,types)
    gen_mod.set_points(points)
    gen_mod.set_datacores_legendre()
    gen_mod.init_coeffcores(coefficients)
    y=gen_mod.forward_generation()
    return gen_mod

## CONTRACT THE COEFFICIENT TENSOR - MAKES ONLY SENSE IN THE PURE MULTIPLICATIVE SETTING
## ! OUTPUT AXIS IS THE SECOND LAST
def contract_cores(mod):
    coeffcores=mod.coeffcores
    outcore=mod.outcore
    contracted=coeffcores[0,0,:,:]
    for k in range(1,mod.nleg-1):
        contracted=np.tensordot(contracted,coeffcores[k,:,:,:],axes=([k-1,0]))
    contracted=np.tensordot(contracted,outcore[:,:,:],axes=([mod.nleg-2,0]))
    return contracted

def compare_contracted_coefficients(mod1,mod2):
    con1=contract_cores(mod1)
    con2=contract_cores(mod2)
    print('Norm of coefficient error, Norm of ground truth coefficients:',np.linalg.norm(con1-2),np.linalg.norm(con2))

def geometry_change_forward(mod):
    mod.forward_generation()
    x_norms=[np.linalg.norm(mod.points[j]) for j in range(mod.ndat)]
    y_norms=[np.linalg.norm(mod.generation[j]) for j in range(mod.ndat)]
    plt.plot(x_norms,y_norms,"ro")
    plt.xlabel('Norm Input Vector')
    plt.ylabel('Norm Generation Vector')
    plt.show()

def geometry_change_parameter(mod,ninit,magnitude=2):
    par_norms=np.zeros(ninit)
    gen_norms=np.zeros(ninit)

    ## AVERAGE
    average=0
    for k in range(ninit):
        mod.init_coeffcores_random(magnitude)
        mod.set_datacores(mod.datatype)
        mod.forward_generation(comgradients=False)

        par_norms[k]=np.linalg.norm(contract_cores(mod))
        gen_norms[k]=np.linalg.norm(mod.generation)
        average+=gen_norms[k]/par_norms[k]
    average=average/ninit

    return average

def geometry_change_visualization(mod,ninit,magnitude=2):
    par_norms=np.zeros(ninit)
    gen_norms=np.zeros(ninit)

    for k in range(ninit):
        mod.init_coeffcores_random(magnitude)
        mod.set_datacores(mod.datatype)
        mod.forward_generation(comgradients=False)

        par_norms[k]=np.linalg.norm(contract_cores(mod))
        gen_norms[k]=np.linalg.norm(mod.generation)


    #mod.forward_generation()
    #x_norms=[np.linalg.norm(mod.points[j]) for j in range(mod.ndat)]
    #y_norms=[np.linalg.norm(mod.generation[j]) for j in range(mod.ndat)]

    #print(average,average/math.sqrt(mod.ndat))

    plt.plot(par_norms,gen_norms,"ro")
    plt.xlabel('Norm Coefficient Tensor')
    plt.ylabel('Norm Generation Vector')
    plt.show()
