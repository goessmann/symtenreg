# Symbolic Tensor Regression - Function learning by tensor recovery methods




## Basic Idea

With the space of sparse symbolic functions parametrized by a tensor network, we formulate interpolation as a tensor recovery problem.  


## Main function models:
*  model_TT: Tensor Train decomposition of the Coefficient Tensor
*  model_CP: CP decomposition of the Coefficient Tensor
*  model_HT (under development): Hierarchial decomposition of the Coefficient Tensor

## Main optimization methods:
* Alternating Least Squares
* Iterative Hard Thresholding 
* Gradient Descent